<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'new_rad');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-%0D,.-1{-<w5ExW.)W;8D/mon68r3K-+GC}*J}ks[L%Zd1:}P25+XQbKt** {d-');
define('SECURE_AUTH_KEY',  's@ !B,<URfUra{UCoV;n|bDSHX0D]jJ_jQVj{bl@eSH:D47`&J8H25Bt}Ok=C|FJ');
define('LOGGED_IN_KEY',    '3lf]H}u//%bJ[>fw&b,fK,3Vn*ZpTcix`67]<V^nU&Hpx;SH++ |R8o33}eJsVT ');
define('NONCE_KEY',        'pg$a,]OSp?j|&k%Df4X6%wfx&9v:YEK F4Stw!]/~pNgQ]M>g1YpIQ|t,f9[9oqC');
define('AUTH_SALT',        ' xG5k<`&08)1o,kj<kSJ)f+!Zf:ug}.#jRQ_GW18Jcu0tu(A1~E=ennM w|WBS4j');
define('SECURE_AUTH_SALT', '5>`w)9QQJ%o3%*VlQ^P?!c:x>PUHo>:.ScQ;|T%E]<:oCmYCd jW08/Q@KXI0k8h');
define('LOGGED_IN_SALT',   'tNyRkK`=nU`%A9(XheVB.90W@J9E8*0f2q4ig};@Ce%X,q&3-]d9fvc+Gi_z kx7');
define('NONCE_SALT',       '`OP>Ro].EfXR>b&M6|`as%1hm]<f:FL$=tk%*t{E/_crL775[H4ouL]0BhC1aCTa');

/**#@-*/
define('WPCF7_AUTOP', false );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ae_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
