<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package framaework
 */

?>
  </div>
  </div>
  <footer class="site-footer color-switcher__white bg-screen" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">

    <div class="site-footer__container">
      <div class="top-footer">
        <div class="vertical-align grid-container--large">
          <h3 class="claim">
            <div class="glitch" data-text="<?php _e('We dare because we care', 'ae'); ?>"><?php _e('We dare because we care', 'ae'); ?></div>
          </h3>
          <div class="call-to-action__contacts">
            <div class="row">
              <div class="row__column desk-6 txt-center">
                <div class="call-to-action__contact call-to-action__contact--email">
                  <h4><?php _e('_Write us', 'ae'); ?></h4>
                  <a href="mailto:hello@we-rad.com" title="<?php _e('Inviaci una mail', 'ae'); ?>" class="call-to-action__element">hello@we-rad.com</a>
                </div>
              </div>
              <div class="row__column desk-6 txt-center">
                <div class="call-to-action__contact call-to-action__contact--phone">
                  <h4><?php _e('_Get in contact', 'ae'); ?></h4>
                  <a href="tel:+39 055 0603050" title="<?php _e('Contattaci al telefono', 'ae'); ?>" class="call-to-action__element">+39 055 0603050</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="middle-footer">
      </div>

      <div class="bottom-footer">
        <div class="social">
          <ul class="inline-li txt-center">
            <li class="social__list">
              <a href="https://www.instagram.com/we_rad/" class="social__link" target="_blank" title="Instagram">
                Instagram
              </a>
            </li>
            <li class="social__list">
              <a href="https://www.facebook.com/weradgroup/" class="social__link" target="_blank" title="Facebbok">
                Facebook
              </a>
            </li>
            <li class="social__list">
              <a href="https://www.linkedin.com/company/rad_group" class="social__link" target="_blank" title="Linkedin">
                Linkedin
              </a>
            </li>
            <li class="social__list">
              <a href="https://vimeo.com/werad" class="social__link" target="_blank" title="Vimeo">
                Vimeo
              </a>
            </li>
          </ul>
        </div>
        <div class="site-footer__info txt-center">
          © <a href="<?php bloginfo('wpurl') ?>" title="<?php bloginfo('name') ?>">WE RAD SRL</a> All rights reserved. P.IVA 06892240489. <a class="site-footer__legals-link" href="<?php echo get_permalink('6579'); ?>" title="Cookies Policy">Cookies Policy</a>
        </div>
      </div>
    </div>

    <canvas class="js--particles particles" data-color="<?php if( checkCookiesSet() == 'negative' ) { echo 'white'; } else { echo 'black'; } ?>" height="1280"></canvas>
  </footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
