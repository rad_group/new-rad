<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package framaework
 */


?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160011558-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-160011558-1');
    </script>

    <meta name="google-site-verification" content="AmSZBOaWplPmm4Mv9n5C0qakMEmjIt0FUQCohqKOdVY" />
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <!-- GMAIL control -->
    <meta name="google-site-verification" content="Dt2cw8VFPB1I9R1Wpx5DeS_AIZZE4CfbfjUy-fGHoyU"/>

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?php bloginfo('template_url'); ?>/favicons/manifest.json">
    <link rel="manifest" href="<?php bloginfo('template_url'); ?>/favicons/site.webmanifest">
    <link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/favicons/safari-pinned-tab.svg" color="#000000">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>

    <!-- START Google Analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-90544096-1', 'auto');
      ga('send', 'pageview');

    </script>
    <!-- END Google Analytics -->

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','https://connect.facebook.net/en_US/fbevents.js');

      fbq('init', '538209749954927');
      fbq('track', "PageView");
      </script>
      <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=538209749954927&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

  </head>

  <body <?php body_class(checkCookiesSet()); ?>>
    <div id="page" class="site">

      <header class="site-header" role="banner" itemscope="itemscope" itemtype="http://schema.org/WebPage">

        <div class="row--stuck columns-middle">

          <div class="row__column mobile-8">
            <div class="site-branding">
                <h1 class="site-branding__site-title">
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="animsition-link" rel="home" title="<?php bloginfo('description'); ?>">
                    <div class="logo black-bg-target">
                      <img class="logo__img svg" src="<?php bloginfo('template_url'); ?>/img/rad-logo-<?php echo checkCookiesSet(); ?>.svg" class="p_n" alt="Rad Group Logo">
                      <h2 Class="logo__title">We Rad</h2>
                    </div>
                  </a>
                </h1>

            </div><!-- .site-branding -->
          </div>

          <div class="row__column mobile-4 txt-right">
            <div class="hamburger js--toggle-menu">
              <div class="hamburger__label">
                <div class="hamburger__label-menu">Menu</div>
              </div>
              <div class="hamburger__bg">
                <span class="hamburger__elements">
                  <span></span>
                </span>
              </div>
            </div>
          </div>

        </div>

        <div class="overlay-menu js--overlay-menu">
          <div class="overlay-menu__dark"></div>
          <div class="overlay-menu__bg">
            <div class="grid-container--menu">
              <div class="grid-container vertical-align">
                <div class="row">
                  <div class="row__column tab-6">
                    <nav class="overlay-menu__nav grid-container--small" itemscope="" itemtype="https://schema.org/SiteNavigationElement" role="navigation">
                      <?php bem_menu('primary', 'primary-menu', 'js--mobile-menu'); ?>
                      <div class="menu-info__language">
                        <?php wp_nav_menu('language', 'language-menu', 'js--mobile-menu'); ?>
                      </div>
                    </nav>
                  </div>
                  <div class="row__column tab-6">
                    <div class="menu-info visible--on-tab">
                      <hgroup class="menu-info__hgroup">
                        <h2 class="menu-info__title">We RAD</h2>
                        <h3 class="menu-info__subtitle"><?php _e('Communication Agency','ae'); ?></h3>
                      </hgroup>
                      <div class="composer__txt-underscore"></div>
                      <ul class="menu-info__ul">
                        <li class="menu-info__li">
                          <h4 class="menu-info__li-title">Email</h4>
                          <a class="menu-info__li-data" href="mailto:hello@we-rad.com" title="RAD email">hello@we-rad.com</a>
                        </li>
                        <li class="menu-info__li">
                          <h4 class="menu-info__li-title"><?php _e('Phone', 'ae'); ?></h4>
                          <a class="menu-info__li-data" href="tel:+39 055 0603050" title="RAD phone">+39 055 0603050</a>
                        </li>
                        <li class="menu-info__li">
                          <h4 class="menu-info__li-title"><?php _e('Address', 'ae'); ?></h4>
                          <address class="menu-info__li-data">Borgo degli Albizi 14<br>50122 - Firenze</address>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </header><!-- #masthead -->
      <div class="site-wrap">
        <div id="content" class="site-content">
