<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main">

  <?php while ( have_posts() ) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?> itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">

    <div class="overflow-hidden color-switcher__black">
      <div class="canvas-container">
        <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
      </div>

      <div class="grid-container--large">

        <header class="page__header single-works__head-alignment">
          <hgroup class="page__hgroup">
            <h1 class="primary-title">
              <div class="glitch-black" data-text="<?php the_title(); ?>">
                <?php the_title(); ?>
              </div>
            </h1>
            <?php if( get_field('post_subtitle') ) : ?>
              <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
            <?php endif; ?>
          </hgroup>
        </header><!-- .page__header -->

        <div class="page__intro formatted-content">
          <?php echo get_field('about_intro_txt'); ?>
        </div>

      </div>

      <div class="single-works__info">
        <div class="grid-container--large">
          <?php
            $hell_no = '';
            $heck_yes = 'hidden';
            $clients = '3';
            if( get_field('hire_check') == true ):
              $hell_no = 'hidden';
              $heck_yes = '';
              $clients = '4 line-up';
            endif;
          ?>
          <div class="row">
            <div class="row__column tab-<?php echo $clients; ?>">
              <div class="single-works__info-single-wrap">
                <?php if( get_field('hire_check') == true ): ?>
                  <h2 class="prefix-title"><?php _e('This gun is for hire:', 'ae'); ?></h2>
                <?php else: ?>
                  <?php if( get_field('works_cliente') ) : ?>
                    <h2 class="prefix-title"><?php _e('Cliente', 'ae'); ?></h2>
                  <?php endif;
                endif; ?>
                <div class="single-works__info-client-txt"><?php echo get_field('works_cliente'); ?></div>
              </div>
            </div>
            <div class="row__column tab-6 <?php echo $hell_no; ?>">
              <div class="single-works__info-single-wrap">
                <?php if( have_rows('works_services') ): ?>
                  <h2 class="prefix-title"><?php _e('Servizi Svolti', 'ae'); ?></h2>
                  <ul class="single-works__services-lists">
                  <?php while ( have_rows('works_services') ) : the_row(); ?>
                    <li class="single-works__services-list">
                      <span class="single-works__services-link">
                        <span>#</span><?php echo get_sub_field('works_single_service'); ?>
                      </span>
                    </li>
                  <?php endwhile; ?>
                  </ul>
                <?php endif; ?>
              </div>
            </div>
            <div class="row__column tab-8 <?php echo $heck_yes; ?>">
              <div class="single-works__info-single-wrap">
                <p>
                  <?php echo get_field('case_description'); ?>
                </p>
              </div>
            </div>
            <?php if( get_field('works_sito_web') ) : ?>
              <div class="row__column tab-3 <?php echo $hell_no; ?>">
                <div class="single-works__info-single-wrap--url">
                  <a class="rad-btn" href="<?php echo get_field('works_sito_web'); ?>" title="<?php _e('Sito web', 'ae'); ?> <?php the_title(); ?>" target="_blank">
                    <?php _e('Visualizza il sito', 'ae'); ?>
                  </a>
                </div>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>

    </div>

    <?php get_template_part('template-parts/composer'); ?>

    <footer class="single-works__pagination grid-container--large color-switcher__black">
      <div class="row--stuck columns-bottom">
        <div class="row__column mobile-5 txt-left">
          <?php if ( get_next_post_link() ) : ?>
            <div class="single-works__pagination-link--prev">
              <?php echo get_next_post_link('<i class="iconae regular ae--arrow-left"><hr class="ae--arrow-trait"></i>%link'); ?>
            </div>
          <?php endif; ?>
        </div>
        <div class="row__column mobile-2 txt-center">
          <div class="all-works">
            <a class="all-works__link animsition-link" href="<?php echo get_permalink('5628'); ?>" title="<?php _e('All works', 'ae'); ?>">
              <span class="all-works__link-squares"></span>
              <span class="all-works__link-label">
                <?php _e('Lavori', 'ae'); ?>
              </span>
            </a>
          </div>
        </div>
        <div class="row__column mobile-5 txt-right">
          <?php if ( get_previous_post_link() ) : ?>
            <div class="single-works__pagination-link--next">
              <?php echo get_previous_post_link('<i class="iconae regular ae--arrow-right"><hr class="ae--arrow-trait"></i>%link'); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </footer>

  </article>

<?php endwhile; ?>

</main><!-- #main -->
<?php get_footer(); ?>
