<?php
if( ! function_exists( 'jobs_create_post_type' ) ) :
  function jobs_create_post_type() {
    $labels = array(
      'name' => 'Jobs',
      'singular_name' => 'Job',
      'add_new' => 'Add New',
      'all_items' => 'All Jobs',
      'add_new_item' => 'Add New Job',
      'edit_item' => 'Edit Job',
      'new_item' => 'New Job',
      'view_item' => 'View Job',
      'search_items' => 'Search Jobs',
      'not_found' => 'No jobs found',
      'not_found_in_trash' => 'No jobs found in trash',
      'parent_item_colon' => 'Parent Job'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'Works of your jobs page',
      'public' => true,
      'has_archive' => true,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'lavora-con-noi-post' ),
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-portfolio',
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 5,
      //'register_meta_box_cb' => 'jobs_add_post_type_metabox'
    );
    register_post_type( 'jobs', $args );
    //flush_rewrite_rules();

  }
  add_action( 'init', 'jobs_create_post_type' );

endif; // end of function_exists()
