<?php
if( ! function_exists( 'humor_create_post_type' ) ) :
  function humor_create_post_type() {
    $labels = array(
      'name' => 'humor',
      'singular_name' => 'humor',
      'add_new' => 'Add New',
      'all_items' => 'All humor',
      'add_new_item' => 'Add New humor',
      'edit_item' => 'Edit humor',
      'new_item' => 'New humor',
      'view_item' => 'View humor',
      'search_items' => 'Search humor',
      'not_found' => 'No humor found',
      'not_found_in_trash' => 'No humor found in trash',
      'parent_item_colon' => 'Parent humor'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'Works of your humor page',
      'public' => true,
      'has_archive' => true,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-smiley',
      'supports' => array(
        'title',
        //'editor',
        //'excerpt',
        //'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        //'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 7,
      //'register_meta_box_cb' => 'clients_add_post_type_metabox'
    );
    register_post_type( 'humor', $args );
    //flush_rewrite_rules();
  }
  add_action( 'init', 'humor_create_post_type' );

endif; // end of function_exists()
