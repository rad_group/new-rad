<?php
if( ! function_exists( 'works_create_post_type' ) ) :
  function works_create_post_type() {
    $labels = array(
      'name' => 'Works',
      'singular_name' => 'Works',
      'add_new' => 'Add New',
      'all_items' => 'All Works',
      'add_new_item' => 'Add New Work',
      'edit_item' => 'Edit Work',
      'new_item' => 'New Work',
      'view_item' => 'View Work',
      'search_items' => 'Search Works',
      'not_found' => 'No works found',
      'not_found_in_trash' => 'No works found in trash',
      'parent_item_colon' => 'Parent Work'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'Works of your clients page',
      'public' => true,
      'has_archive' => true,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'lavori-agenzia' ),
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-art',
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 5,
      //'register_meta_box_cb' => 'works_add_post_type_metabox'
    );
    register_post_type( 'works', $args );
    //flush_rewrite_rules();

  }
  add_action( 'init', 'works_create_post_type' );

endif; // end of function_exists()
