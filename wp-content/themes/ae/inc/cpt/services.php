<?php
if( ! function_exists( 'services_create_post_type' ) ) :
  function services_create_post_type() {
    $labels = array(
      'name' => 'Services',
      'singular_name' => 'Service',
      'add_new' => 'Add New',
      'all_items' => 'All Services',
      'add_new_item' => 'Add New Service',
      'edit_item' => 'Edit Service',
      'new_item' => 'New Service',
      'view_item' => 'View Service',
      'search_items' => 'Search Services',
      'not_found' => 'No services found',
      'not_found_in_trash' => 'No services found in trash',
      'parent_item_colon' => 'Parent Service'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'Works of your services page',
      'public' => true,
      'has_archive' => true,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'servizi-agenzia' ),
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-screenoptions',
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 5,
      //'register_meta_box_cb' => 'services_add_post_type_metabox'
    );
    register_post_type( 'services', $args );
    //flush_rewrite_rules();

    /* Register custom taxonomy - services category */
    register_taxonomy( 'services_category',
      'services',
      array( 'hierarchical' => true,
        'label' => 'Service categories'
      )
    );
  }
  add_action( 'init', 'services_create_post_type' );

endif; // end of function_exists()
