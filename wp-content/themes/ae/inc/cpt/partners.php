<?php
if( ! function_exists( 'partners_create_post_type' ) ) :
  function partners_create_post_type() {
    $labels = array(
      'name' => 'partners',
      'singular_name' => 'partners',
      'add_new' => 'Add New',
      'all_items' => 'All partners',
      'add_new_item' => 'Add New partners',
      'edit_item' => 'Edit partners',
      'new_item' => 'New partners',
      'view_item' => 'View partners',
      'search_items' => 'Search partners',
      'not_found' => 'No partners found',
      'not_found_in_trash' => 'No partners found in trash',
      'parent_item_colon' => 'Parent partners'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'Works of your partners page',
      'public' => true,
      'has_archive' => true,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-universal-access',
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 5,
      //'register_meta_box_cb' => 'partners_add_post_type_metabox'
    );
    register_post_type( 'partners', $args );
    //flush_rewrite_rules();

    /* Register custom taxonomy - partners category */
    register_taxonomy( 'partners_category',
      'partners',
      array( 'hierarchical' => true,
        'label' => 'partners categories'
      )
    );
  }
  add_action( 'init', 'partners_create_post_type' );

endif; // end of function_exists()
