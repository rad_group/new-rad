<?php
if( ! function_exists( 'clients_create_post_type' ) ) :
  function clients_create_post_type() {
    $labels = array(
      'name' => 'clients',
      'singular_name' => 'clients',
      'add_new' => 'Add New',
      'all_items' => 'All clients',
      'add_new_item' => 'Add New clients',
      'edit_item' => 'Edit clients',
      'new_item' => 'New clients',
      'view_item' => 'View clients',
      'search_items' => 'Search clients',
      'not_found' => 'No clients found',
      'not_found_in_trash' => 'No clients found in trash',
      'parent_item_colon' => 'Parent clients'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'Works of your clients page',
      'public' => true,
      'has_archive' => true,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-universal-access-alt',
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 5,
      //'register_meta_box_cb' => 'clients_add_post_type_metabox'
    );
    register_post_type( 'clients', $args );
    //flush_rewrite_rules();

    /* Register custom taxonomy - clients category */
    register_taxonomy( 'clients_category',
      'clients',
      array( 'hierarchical' => true,
        'label' => 'clients categories'
      )
    );
  }
  add_action( 'init', 'clients_create_post_type' );

endif; // end of function_exists()
