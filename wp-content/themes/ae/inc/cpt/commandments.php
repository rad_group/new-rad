<?php
if( ! function_exists( 'commandments_create_post_type' ) ) :
  function commandments_create_post_type() {
    $labels = array(
      'name' => 'Commandments',
      'singular_name' => 'Commandments',
      'add_new' => 'Add New',
      'all_items' => 'All commandments',
      'add_new_item' => 'Add New Commandments',
      'edit_item' => 'Edit Commandments',
      'new_item' => 'New Commandments',
      'view_item' => 'View Commandments',
      'search_items' => 'Search commandments',
      'not_found' => 'No commandments found',
      'not_found_in_trash' => 'No commandments found in trash',
      'parent_item_colon' => 'Parent Commandments'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'Works of your commandments page',
      'public' => true,
      'has_archive' => true,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-book-alt',
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 5,
      //'register_meta_box_cb' => 'commandments_add_post_type_metabox'
    );
    register_post_type( 'commandments', $args );
    //flush_rewrite_rules();

    /* Register custom taxonomy - commandments category */
    register_taxonomy( 'commandments_category',
      'commandments',
      array( 'hierarchical' => true,
        'label' => 'Commandments categories'
      )
    );
  }
  add_action( 'init', 'commandments_create_post_type' );

endif; // end of function_exists()
