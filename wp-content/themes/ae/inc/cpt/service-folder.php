<?php
if( ! function_exists( 'servicefolder_create_post_type' ) ) :
  function servicefolder_create_post_type() {
    $labels = array(
      'name' => 'Services Folder',
      'singular_name' => 'Service Folder',
      'add_new' => 'Add New',
      'all_items' => 'All Services Folder',
      'add_new_item' => 'Add New Services Folder',
      'edit_item' => 'Edit Services Folder',
      'new_item' => 'New Services Folder',
      'view_item' => 'View Services Folder',
      'search_items' => 'Search Services Folders',
      'not_found' => 'No Services Folder found',
      'not_found_in_trash' => 'No Services Folder found in trash',
      'parent_item_colon' => 'Parent Services Folder'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'Contents folder of services page',
      'public' => true,
      'has_archive' => true,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => false,
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-category',
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 5,
      //'register_meta_box_cb' => 'servicefolder_add_post_type_metabox'
    );
    register_post_type( 'servicefolder', $args );
    //flush_rewrite_rules();

  }
  add_action( 'init', 'servicefolder_create_post_type' );

endif; // end of function_exists()
