<?php
/**
 * The template for displaying authors pages
 *
 * @link https://codex.wordpress.org/Author_Templates
 *
 * @package framaework
 */

get_header(); ?>

<?php
$user_id = get_the_author_meta('ID');
$user_nice = get_the_author_meta('nickname');
?>

<main id="main" class="site-main" role="main">

  <section class="blog-author">
    <div class="overflow-hidden">
      <div class="canvas-container">
        <canvas class="js--particles particles" data-color="<?php if( checkCookiesSet() == 'negative' ) { echo 'black'; } else { echo 'white'; } ?>" height="1280"></canvas>
      </div>

      <div class="blog-author__wrap">
        <div class="grid-container">
          <div class="row">

            <div class="row__column tab-4 desk-3 txt-center">
              <div class="blog-author__avatar">
                <?php if ( get_field('author_avatar', 'user_'.$user_id) ) : ?>
                  <img class="blog-author__avatar-img" src="<?php echo get_field('author_avatar', 'user_'.$user_id); ?>" alt="Immagine profilo di <?php echo get_the_author_meta('nickname'); ?>">
                <?php else : ?>
                  <img class="blog-author__avatar-img" src="<?php bloginfo('template_directory'); ?>/img/RAD-wp-avatar.png" alt="Immagine profilo di RAD">
                <?php endif; ?>
              </div>
            </div>

            <div class="row__column tab-8 desk-9">
              <h3 class="blog-author__nickname"><?php echo get_the_author_meta('nickname'); ?></h3>
              <?php if ( get_field('author_job', 'user_'.$user_id) ) : ?>
                <h4 class="blog-author__job">
                  <?php echo get_field('author_job', 'user_'.$user_id); ?>
                </h4>
              <?php endif; ?>

              <nav class="blog-author__social">
                <?php if ( get_the_author_meta( 'user_url', $post->post_author ) ) : ?>
                  <a href="<?php echo get_the_author_meta( 'user_url', $post->post_author ); ?>" title="Sito personale di <?php echo get_the_author_meta('nickname'); ?>" target="_blank">
                    <i class="iconae light ae--globe"></i>
                  </a>
                <?php endif; ?>

                <?php if ( get_field('social_author_linkedin', 'user_'.$user_id) ) : ?>
                  <a href="<?php echo get_field('social_author_linkedin', 'user_'.$user_id); ?>" title="Profilo Linkedin personale di <?php echo get_the_author_meta('nickname'); ?>" target="_blank">
                    <i class="iconae light ae--linkedin-square"></i>
                  </a>
                <?php endif; ?>

                <?php if ( get_field('social_author_instagram', 'user_'.$user_id) ) : ?>
                  <a href="<?php echo get_field('social_author_instagram', 'user_'.$user_id); ?>" title="Profilo Instagram personale di <?php echo get_the_author_meta('nickname'); ?>" target="_blank">
                    <i class="iconae light ae--instagram"></i>
                  </a>
                <?php endif; ?>

                <?php if ( get_the_author_meta( 'googleplus',$post->post_author) ) : ?>
                  <a href="<?php echo get_the_author_meta( 'googleplus',$post->post_author);?>" title="Pagina Google Plus di <?php echo get_the_author_meta('nickname'); ?>" target="_blank" rel="me">
                    <i class="iconae light ae--google-plus-square"></i>
                  </a>
                <?php endif; ?>

              </nav>

              <div class="blog-author__desc formatted-content">
                <p>
                  <?php echo get_the_author_meta("description"); ?>
                </p>
              </div>

            </div>
          </div>

        </div>
      </div>

    </div>
  </section>

  <div class="grid-container--large">
    <div class="row blog-row">

      <div class="row blog-row">
        <?php
        if ( have_posts() ) :
          $postNumber = 0;

          /* Start the Loop */
          while ( have_posts() ) : the_post(); ?>

          <?php
          if ( $postNumber <= 2 ) {
            $postNumber ++;
          } else {
            $postNumber = 0;
            $postNumber ++;
          }?>

            <div class="row__column tab-6 desk-4 <?php if ( $postNumber == 2 ) { echo 'upper-post';} ?>">
              <?php get_template_part( 'template-parts/content', 'post' ); ?>
            </div>

          <?php endwhile;

          the_posts_navigation();

        else :

          get_template_part( 'template-parts/content', 'none' );

        endif; ?>
    </div>
  </div>

</main>
<?php get_footer(); ?>
