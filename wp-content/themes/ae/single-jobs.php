<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main">
  <?php while ( have_posts() ) : the_post(); ?>
    <div class="overflow-hidden">
      <div class="canvas-container">
        <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
      </div>

      <div class="grid-container--small">
        <header class="page__header">
          <hgroup class="page__hgroup">
            <h1 class="prefix-title"><?php _e('Lavora con noi', 'ae'); ?></h1>
            <?php the_title( '<h2 class="secondary-title">', '</h2>' ); ?>
          </hgroup>
        </header><!-- .page__header -->

      </div>
    </div>

    <section class="single-job">
      <div class="grid-container">
        <div class="formatted-content jobs-intro page__content">
          <?php the_content(); ?>
        </div>
      </div>

      <div class="job-skills">
        <div class="grid-container">
          <div class="row">
            <div class="row__column tab-6">
              <div class="formatted-content">
                <?php if( have_rows('jobs_list_foundamentals') ): ?>

                  <h4 class="jobs-list__title"><?php _e('Requisiti fondamentali:', 'ae'); ?></h4>

                  <ul class="jobs-list">
                  <?php while ( have_rows('jobs_list_foundamentals') ) : the_row(); ?>

                    <li class="jobs-list__single"><?php the_sub_field('jobs_list_foundamentals_txt'); ?></li>

                  <?php endwhile; ?>
                  </ul>

                <?php endif; ?>
              </div>
            </div>
            <div class="row__column tab-6">
              <div class="formatted-content">
                <?php if( have_rows('jobs_list_ottimali') ): ?>

                  <h4 class="jobs-list__title"><?php _e('Requisiti ottimali:', 'ae'); ?></h4>

                    <ul class="jobs-list">
                  <?php while ( have_rows('jobs_list_ottimali') ) : the_row(); ?>

                    <li class="jobs-list__single"><?php the_sub_field('jobs_list_ottimali_txt'); ?></li>

                  <?php endwhile; ?>
                  </ul>

                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="jobs-form contact-form grid-container">

      <?php
      function cf7_dynamic_select_do_example1($choices, $args=array()) {

        $choices = array(
          get_the_title() => get_the_title()
        );

        return $choices;
      }

      add_filter('wpcf7_dynamic_select_example1', 'cf7_dynamic_select_do_example1', 10, 2);

      ?>

      <div class="contact-form__flat">
        <div class="formatted-content">
          <h2><?php _e('Inviaci la tua richiesta', 'ae'); ?></h2>
          <p><?php _e('Inviaci la tua richiesta, sarà presa in carico dal nostro team. Seleziona dal menu a tendina una delle nostre posizioni aperte, se prensenti, altrimenti condividici semplicemente il tuo curriculum e le tue esperienze professionali.', 'ae'); ?></p>
        </div>
        <?php echo do_shortcode('[contact-form-7 id="5299" title="Jobs Form"]'); ?>
      </div>
    </section>
  <?php endwhile; ?>
</main><!-- #main -->
<?php get_footer(); ?>
