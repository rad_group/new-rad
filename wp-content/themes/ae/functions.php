<?php
/**
 * framaework functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package framaework
 */

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Custom functions that act independently of the theme templates.
require get_template_directory() . '/inc/extras.php';

// Include Elements
require get_template_directory() . '/inc/file_calling.php';

// Include Elements
require get_template_directory() . '/inc/cookies.php';

// Merge CSS and JS files into one
// if ( !is_admin() && $pagenow != 'wp-login.php' ) {
//   require get_template_directory() . '/inc/merging.php';
//   require get_template_directory() . '/inc/minification.php';
// }

// Include Custom post type files
require get_template_directory() . '/inc/cpt/services.php';
require get_template_directory() . '/inc/cpt/commandments.php';
require get_template_directory() . '/inc/cpt/clients.php';
require get_template_directory() . '/inc/cpt/partners.php';
require get_template_directory() . '/inc/cpt/humor.php';
require get_template_directory() . '/inc/cpt/jobs.php';
require get_template_directory() . '/inc/cpt/works.php';
require get_template_directory() . '/inc/cpt/service-folder.php';

// Include Widgets
require get_template_directory() . '/inc/widgets.php';

// Change the menu layout with Bem Semantic
require get_template_directory() . '/inc/bem_menu.php';

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
if ( ! function_exists( 'ae_setup' ) ) :

  function ae_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on framaework, use a find and replace
     * to change 'ae' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'ae', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
      'primary' => __( 'Primary Menu', 'ae' ),
      'language' => __( 'Language Menu', 'ae' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ) );

    // Set Excerpt length
    function custom_excerpt_length( $length ) {
      return 36;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

    // Set Read More at the excerpt end
    function new_excerpt_more( $more ) {
      return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'ae') . '</a>';
    }
    add_filter( 'excerpt_more', 'new_excerpt_more' );

  }

endif;
add_action( 'after_setup_theme', 'ae_setup' );
