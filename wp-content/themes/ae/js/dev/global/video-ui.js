// Document Ready
$( document ).ready(function() {

  play_video();
  about_video();

}); //END Document Ready


function play_video() {
  if ( $('.js--video-ui').length ) {
    $('.video-ui__playpause').click(function () {
      var element = $(this).parent().find(".video-ui__video").get(0);
      if( element.paused ){
        if (element.requestFullscreen) {
          element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
          element.mozRequestFullScreen(); // Firefox
        } else if (element.webkitRequestFullscreen) {
          element.webkitRequestFullscreen(); // Chrome and Safari
        }
        $('.video-ui__video').attr('controls','');

        element.play();
        //$(this).children(".video-ui__playpause").html('<i class="iconae regular ae--pause-circle"></i>');
      } else {
        $('.video-ui__video').removeAttr('controls');
        element.pause();
        //$(this).children(".video-ui__playpause").html('<i class="iconae regular ae--play-circle"></i>');
      }
    });
    $('.video-ui__video').bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
      var that = $(this);
      var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
      var event = state ? 'FullscreenOn' : 'FullscreenOff';

      if ( event == 'FullscreenOff' ) {
        that.get(0).pause();
        //$(".video-ui__playpause").html('<i class="iconae regular ae--play-circle"></i>');
        $('.video-ui__video').removeAttr('controls');
      }

    });
  }
};


//function about_video(){
  // $('.js--about-video').click(function(){
  //   console.log('click');
  //   $('.height-screen__2-3').toggleClass('get-out');
  //
  //   if( $(this).prop('muted') ){
  //     $('.site').addClass('slide');
  //     $('html').addClass('lock');
  //     $(this).prop('muted', false);
  //   }else{
  //     $('.site').removeClass('slide');
  //     $('html').removeClass('lock');
  //     $(this).prop('muted', true);
  //   }
  // });
//}


function about_video() {
  if ( $('.js--about-video-ui').length ) {
    $('.about-video-ui__playpause').click(function () {
      var element = $(this).parent().find(".about-video-ui__video").get(0);
      if (element.requestFullscreen) {
        element.requestFullscreen();
      } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen(); // Firefox
      } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen(); // Chrome and Safari
      }

      element.play();
    });
    $('.about-video-ui__video').bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
      var that = $(this);
      var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
      var event = state ? 'FullscreenOn' : 'FullscreenOff';


    });
  }
};
