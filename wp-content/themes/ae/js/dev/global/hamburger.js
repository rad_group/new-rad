// Document Ready
$( document ).ready(function() {

  $('.js--toggle-menu').click(function() {
    $('body').toggleClass('menu-opened');
    $(this).toggleClass('is-active');
    $('.js--overlay-menu').toggleClass('opened');

    if ( $(this).hasClass('is-active') ) {
      $('.hamburger__label-menu').html('Close');
    } else {
      $('.hamburger__label-menu').html('Menu');
    }
  });

  $(document).keyup(function(e) {
    if (e.keyCode == 27) {

      $('.js--toggle-menu').removeClass('is-active');
      $('.js--overlay-menu').removeClass('opened');
      $('body').removeClass('menu-opened');

    }
});

}); //END Document Ready
