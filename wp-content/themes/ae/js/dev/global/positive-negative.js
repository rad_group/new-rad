// Document Ready
$( document ).ready(function() {

  positive_negative();

});

function set_humor(set,ck,prev) {
  jQuery.ajax({
    url : humor_fun.ajax_url,
    type : 'post',
    data : {
      action : 'change_humor',
      humor : set,
      cookie : ck,
      previous : prev
    },
    success : function(response) {
	    //console.log(response);
    },
    error: function(error){
      console.log(error);
    }
  });

}

function get_src(el,set) {
  jQuery.ajax({
    url : setimages_fun.ajax_url,
    type : 'post',
    data : {
      action : 'change_images',
      ID : el.data('id'),
      set : set,
      acf : el.data('acf')
    },
    success : function(response) {
      el.attr('src',response);
    },
    error: function(error){
      console.log(error);
    }
  });

}

function positive_negative_image(src,rep) {

  $("img.p_n[src*='"+src+"']").each(function(){
     $(this).attr('src',$(this).attr('src').replace(src, rep));
  });

  $("img.p_n_dynamic").each(function(){
    result = get_src($(this),rep);
  });

  $(".p_n_video").each(function(){
    if ($(this).find('video').attr('poster').indexOf(src) >= 0){
     new_src = $(this).find('video').attr('poster').split('poster-'+src+'.jpg');
     new_poster = new_src[0]+'poster-'+rep+'.jpg';
     new_webm = new_src[0]+rep+'.webm';
     new_mp4 = new_src[0]+rep+'.mp4';

     $(this).empty();
     video = '<video playsinline autoplay muted loop poster="'+new_poster+'" class="bgvid"><source src="'+new_webm+'" type="video/webm"><source src="'+new_mp4+'" type="video/mp4"></video>';
     $(this).html(video);
     var video = $('.bgvid')[0];
     video.load();
     video.play();
    }
  });
}

function positive_negative_particles(ck) {

  if( $('.js--particles').length ) {
    if (((Cookies.get('p_n')) && (ck != Cookies.get('p_n')))||((!Cookies.get('p_n'))&&(ck!='positive'))) {
      $( ".js--particles" ).each(function( index ) {
        var current_color = $( this ).data('color');
        if(current_color=='black'){
          $( this ).data('color','white');
        }else{
          $( this ).data('color','black');
        }
      });
    }
  }

}


function positive_negative() {
  $('.js--p-n').click(function(){
    if ( $(this).hasClass('js--positive-btn') ) {
      $('body').removeClass('negative');
      $('body').addClass('positive');

      if(Cookies.get('p_n')){ck=1;}else{ck=0;}
      set_humor('positive',ck,Cookies.get('p_n'));

      positive_negative_particles('positive');
      setCollectionCookie('positive');
      positive_negative_image('negative', 'positive');

    } else if ( $(this).hasClass('js--negative-btn') ) {
      $('body').removeClass('positive');
      $('body').addClass('negative');

      if(Cookies.get('p_n')){ck=1;}else{ck=0;}
      set_humor('negative',ck,Cookies.get('p_n'));

      positive_negative_particles('negative');
      setCollectionCookie('negative');
      positive_negative_image('positive', 'negative');
    }
    if(typeof ctx_general != 'undefined'){
      location.reload();
    }
    $('.js--preload').removeClass('loaded');

    // Close popup
    setTimeout(function() {
      $('.js--popup').removeClass('active');
      $('body').removeClass('popup-opened');
    }, 300);

    setTimeout(function() {
      $('.js--preload').addClass('loaded');
    }, 1000);

  });

  function setCollectionCookie(cookie) {
    Cookies.remove('p_n');
    Cookies.set('p_n', cookie, { expires: 7 });
  }

}
