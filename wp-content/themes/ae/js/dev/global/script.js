// Document Ready
$( document ).ready(function() {

  if ( $(".js--lazy").length ) {
    $(function() {
      $(".js--lazy").lazyload({
        effect : "fadeIn"
      });
    });
  }
  
  // $('.js--grid').masonry({
  //   // columnWidth: 200,
  //   itemSelector: '.js--grid-item'
  // });

}); //END Document Ready
