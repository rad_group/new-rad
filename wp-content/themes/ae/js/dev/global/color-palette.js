// Document Ready
$( document ).ready(function() {

  $('.color-palette__box').hover(
    function(){
      $colorPicker = $(this).children().data('color');
      $('.color-palette').css('background-color', $colorPicker);
      if( $(this).hasClass('contrast') ){
        $('.color-palette').addClass('high-contrast color-switcher__black');
      }
  },function(){
    $('.color-palette').css('background-color', '#ffffff');
    $('.color-palette').removeClass('high-contrast');
  });

}); //END Document Ready
