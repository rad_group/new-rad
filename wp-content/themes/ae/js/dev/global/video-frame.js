/**
* Frame-by-frame video animation with ScrollMagic and GSAP
*
* Note that your web server must support byte ranges (most do).
* Otherwise currentTime will always be 0 in Chrome.
* See here: http://stackoverflow.com/a/5421205/950704
* and here: https://bugs.chromium.org/p/chromium/issues/detail?id=121765
*/

if ( $('.js--dna').length ) {
   var video = document.getElementById('video');
   var long = document.getElementById('long');
   var scrollpos = 0;
   var lastpos = 0;
   var controller = new ScrollMagic.Controller();
   var scene = new ScrollMagic.Scene({
     triggerElement: long,
     triggerHook: 0 });
   var frameRate=180;

  function getCurrentVideoFrame() {
      curTime = video.duration;
      theCurrentFrame=Math.floor(curTime*frameRate);

      return theCurrentFrame;
  }

   var startScrollAnimation = function startScrollAnimation() {
     scene.
     setPin("#long").
     addTo(controller).
     duration(getCurrentVideoFrame()+window.innerHeight).
     //addIndicators({name: "video scene", colorEnd: "#FF0000"}). // add indicators (requires plugin)
     on("progress", function (e) {//console.log(e);
       scrollpos = e.progress;
     });
     console.log(scene.duration());
     setInterval(function () {
       requestAnimationFrame(function () {

          var accelamount = 0.0001;
          //console.log(window.innerHeight);
          var targetscrollpos = (window.pageYOffset)/172;
          var scrollpos = targetscrollpos;
          var scrollpos2 = targetscrollpos-window.innerHeight;

          scrollpos2 += ((targetscrollpos-window.innerHeight) - scrollpos2)*accelamount;
          video.currentTime = scrollpos;
          //console.log(scrollpos);
          video.pause();
       });
     }, 50);

   };

   var preloadVideo = function preloadVideo(v, callback) {
     var ready = function ready() {
       v.removeEventListener('canplaythrough', ready);
       video.pause();
       var i = setInterval(function () {
         if (v.readyState > 3) {
           clearInterval(i);
           video.currentTime = 0;
           callback();
         }
       }, 50);
     };
     v.addEventListener('canplaythrough', ready, false);
     //v.play();
   };

   preloadVideo(video, startScrollAnimation);

 }
