$( document ).ready(function() {

  archive_popup();

});


function archive_popup() {
  if ( $('.js--active-archive-popup').length ) {
    $('.js--active-archive-popup').click(function(){
      $('.site-wrap').addClass('archive-opened');
      $('.js--archive-poup').addClass('enable');
    });

    $(document).keyup(function(e) {
      if (e.keyCode == 27) {
        $('.site-wrap').removeClass('archive-opened');
        $('.js--archive-poup').removeClass('enable');
      }
    });
  }
}
