// Document Ready
$( document ).ready(function() {
  logo_height();
}); //END Document Ready

$( window ).resize(function() {
  logo_height();
});

function logo_height() {

  if ( $('.js--otto').length ) {
    var otto_w = $('.js--otto').outerWidth();
    var otto_h = otto_w;
    var first_h = otto_h/2;

    $('.js--otto').css('height', otto_w);

    $.each( $('.home-clients__logo'), function() {
      $(this).css('height', first_h);
    });
  }

}
