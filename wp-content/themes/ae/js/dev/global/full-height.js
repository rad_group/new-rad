// Document Ready
$( document ).ready(function() {
  if ( $('.js--full-height-content').length ) {
    full_height_content();
  }

  if ( $('.js--full-height').length ) {
    full_height();
  }
});

$( window ).resize(function() {
  if ( $('.js--full-height-content').length ) {
    full_height_content();
  }

  if ( $('.js--full-height').length ) {
    full_height();
  }
});  //END Window Resize

function full_height_content() {
  var header = $('.site-header');
  var content = $('.no-touch .js--full-height-content');
  var footer = $('.site-footer');

  var window_h = $(window).height();

  var header_h = $('.site-header').outerHeight();
  var content_h = $('.js--full-height-content').outerHeight();
  var footer_h = $('.site-footer').outerHeight();

  var full_h = window_h - header_h - footer_h - 174;

  $('.js--full-height-content').css('min-height', full_h);
}

function full_height() {
  var header = $('.site-header');
  var content = $('.no-touch .js--full-height');
  var footer = $('.site-footer');

  var window_h = $(window).height();

  var header_h = $('.site-header').outerHeight();
  var content_h = $('.js--full-height').outerHeight();
  var footer_h = $('.site-footer').outerHeight();

  var full_h = window_h - header_h;

  $('.js--full-height').css('min-height', full_h);
}
