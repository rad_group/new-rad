// Document Ready
$( document ).ready(function() {
  law_trigger();
  fixed_number();
});

$(window).on('scroll', function () {
  law_trigger();
});

function law_trigger() {
  if ( $('.js--laws-number').length ) {
    number = $('.js--laws-number');
    number_offset = number.offset().top;

    single_law = $(".js--law-trigger");

    single_law.each(function() {
      single_law_offset = $(this).offset().top;
      single_law_h = $(this).height();
      if ( number_offset >= single_law_offset - 30 && number_offset <= single_law_offset + single_law_h ) {
        data_law = $(this).data('law');
        //console.log(data_law);
        number.html(data_law);
      }
    });
  }
}

function fixed_number() {
  if ( $('.js--laws-number').length ) {
    number_pos = number.position().top;
    $(window).on('scroll', function () {
      header_h = $('.site-header').outerHeight();

      scroll_top = $(window).scrollTop();
      number = $('.js--laws-number');
      number_offset = number.offset().top;
      number_h = number.height();

      number_total = scroll_top + number_pos + number_h;

      list = $('.js--laws__list');
      list_offset = list.offset().top;
      list_h = list.height();
      list_total = list_offset + list_h;

      last_offset = $('.js--law-trigger:last-child').offset().top;

      console.log(last_offset);

      if (number_total >= list_total) {
        number.css('top', last_offset).css('position', 'absolute');
      } else {
        number.css('top', 'auto').css('position', 'fixed');
      }
    });
  }
}
