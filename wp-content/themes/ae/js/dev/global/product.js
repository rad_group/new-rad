jQuery(document).ready(function($){

  var productViewer = function(element) {
    this.element = element;
    this.handleContainer = this.element.find('.cd-product-viewer-handle');
    this.handleFill = this.handleContainer.children('.fill');
    this.handle = this.handleContainer.children('.handle');
    this.imageWrapper = this.element.find('.product-viewer');
    this.slideShow = this.imageWrapper.children('.product-viewer__film');
    this.frames = this.element.data('frame');
    //increase this value to increase the friction while dragging on the image - it has to be bigger than zero
    this.friction = this.element.data('friction');
    this.visibleFrame = 0;
    this.loaded = false;
    this.animating = false;
    this.xPosition = 0;
    this.loadFrames();
  }

  // Extract img from data-image attribute
  var data_image = $('.product-viewer__film').attr('data-image');
  var data_frame = $('.cd-product-viewer-wrapper').attr('data-frame');

  $('.product-viewer__film')
  .css('background-image','url(' + data_image + ')')
  .css('width', data_frame * 100 + '%');

  /* @TODO Rendere dinamica la percentuale di ingrandimento degli sprite (n° frame*100) */

  productViewer.prototype.loadFrames = function() {
    var self = this,
      imageUrl = this.slideShow.data('image'),
      newImg = $('<img/>');
    this.loading('0.5');
    //you need this to check if the image sprite has been loaded
    newImg.attr('src', imageUrl).load(function() {
      $(this).remove();
        self.loaded = true;
      }).each(function(){
        image = this;
      if(image.complete) {
          $(image).trigger('load');
        }
    });
  }

  productViewer.prototype.loading = function(percentage) {
    var self = this;
    transformElement(this.handleFill, 'scaleX('+ percentage +')');
    setTimeout(function(){
      if( self.loaded ){
        //sprite image has been loaded
        self.element.addClass('loaded');
        transformElement(self.handleFill, 'scaleX(1)');
        self.dragImage();
      } else {
        //sprite image has not been loaded - increase self.handleFill scale value
        var newPercentage = parseFloat(percentage) + .1;
        if ( newPercentage < 1 ) {
          self.loading(newPercentage);
        } else {
          self.loading(parseFloat(percentage));
        }
      }
    }, 500);
  }

  /* START dragImage() OK */
  productViewer.prototype.dragImage = function() {
    //implement image draggability
    var self = this;
    self.slideShow.on('mousedown vmousedown', function (e) {
          self.slideShow.addClass('cd-draggable');
          var containerOffset = self.imageWrapper.offset().left,
              containerWidth = self.imageWrapper.outerWidth(),
              minFrame = 0,
              maxFrame = self.frames - 1;

          self.xPosition = e.pageX;

          self.element.on('mousemove vmousemove', function (e) {
            if( !self.animating) {
              self.animating =  true;
              ( !window.requestAnimationFrame )
                ? setTimeout(function(){self.animateDraggedImage(e, containerOffset, containerWidth);}, 100)
                : requestAnimationFrame(function(){self.animateDraggedImage(e, containerOffset, containerWidth);});
            }
          }).one('mouseup vmouseup', function (e) {
              self.slideShow.removeClass('cd-draggable');
              self.element.off('mousemove vmousemove');
          });

          e.preventDefault();

      }).on('mouseup vmouseup', function (e) {
          self.slideShow.removeClass('cd-draggable');
      });
  }
  /* END dragImage() OK */

  productViewer.prototype.animateDraggedImage = function(e, containerOffset, containerWidth) {
    var self = this;
    var leftValue = self.xPosition - e.pageX;
        var widthValue = Math.ceil( (leftValue) * 100 / ( containerWidth * self.friction ));
        var frame = (widthValue * (self.frames-1))/100;
        if( frame > 0 ) {
          frame = Math.floor(frame);
        } else {
          frame = Math.ceil(frame);
        }
        var newFrame = self.visibleFrame + frame;

        if (newFrame < 0) {
            newFrame = self.frames - 1;
        } else if (newFrame > self.frames - 1) {
            newFrame = 0;
        }

        if( newFrame != self.visibleFrame ) {
          self.visibleFrame = newFrame;
          self.updateFrame();
          self.xPosition = e.pageX;
        }

        self.animating =  false;
  }

  var roll = 0;
  var scrolled_frames = parseInt(100/data_frame);
  var start_roll = 0;
  var step_roll = 3;

  var $inner_h = $('.scrollbar-inner').height();
  $('html').css('height', $inner_h);


  $('.scrollbar-inner').addClass('scrolled');

  var positionY = 0;

  $(window).scroll(function(){

    $viewportHeight = $(window).outerHeight();
    $viewportSection = $viewportHeight / 2.2;
    if( $('.composer__block').hasClass('js--flex-360') ){
      mobileScroll($viewportSection);
    }

  });

  function mobileScroll(h) {

    var scroll = $(window).scrollTop();
    var spinStart = $('.cd-product-viewer-wrapper').offset().top - h;
    if( scroll >= spinStart ){
      start_roll = start_roll + 1;
      if( step_roll == start_roll ) {
        if( scroll >= positionY && roll <= 0 ) {
          if ( roll > -100 ) {
            roll = roll - 5;
          }
        } else if ( scroll < positionY && roll < 0 ) {
          roll = roll + 5;
        }
        $('.product-viewer__film').css('transform', 'translateX(' + roll + '%)');
        start_roll = 0;
      }
      positionY = scroll <= 0 ? 0 : scroll
    } else {
      $('.product-viewer__film').css('transform', 'translateX(0%)');
    }

  }



  productViewer.prototype.updateFrame = function() {
    var transformValue = - (100 * this.visibleFrame/this.frames);
    transformElement(this.slideShow, 'translateX('+transformValue+'%)');
  }

  var positionY = 0;
  deltaScroll = 0;

  // function cosoSpinner(e, h){
  //
  //   var scroll = $(window).scrollTop();
  //   var spinStart = $('.cd-product-viewer-wrapper').offset().top - h;
    // console.log(spinStart);

    // if( scroll >= spinStart ){
    //   start_roll = start_roll + 1;
    //   // console.log('vera');
    //   // console.log(start_roll);
    //   if( step_roll == start_roll ) {
    //     if( scroll >= spinStart ){
    //       // console.log('scroll: '+scroll+ ' target: '+ spinStartMobile);
    //       // console.log(event.pageY);
    //       if( e.deltaY >= 0 && roll < 0 ) {
    //         //Scroll verso l'alto
    //         roll = roll + 5;
    //       } else if ( e.deltaY < 0 && roll > -100 ) {
    //         //Scroll verso il basso
    //         roll = roll - 5;
    //       } else {
    //       // if ( roll == -100 ) {
    //         //   scroll_status(1);
    //         // } else if ( roll > -100 && scroll == 0 ) {
    //         //   scroll_status(0);
    //       // }
    //       }
    //       // console.log(roll);
    //       $('.product-viewer__film').css('transform', 'translateX(' + roll + '%)');
    //       start_roll = 0;
    //       // console.log('DELTA-Y: ' + e.deltaY, ' DELTA-Factor: ' + e.deltaFactor);
    //       // console.log(scroll);
    //     } else {
    //       // $('.product-viewer__film').css('transform', 'translateX(0%)');
    //       // start_roll = 0;
    //     }
    //   }
    // } else {
    //   $('.product-viewer__film').css('transform', 'translateX(0%)');
    //   // console.log('falsa!!!');
    // }




  // }

  function transformElement(element, value) {
    element.css({
      '-moz-transform': value,
      '-webkit-transform': value,
      '-ms-transform': value,
      '-o-transform': value,
      'transform': value,
    });
  }

  var productToursWrapper = $('.cd-product-viewer-wrapper');
  productToursWrapper.each(function(){
    new productViewer($(this));
  });

});
