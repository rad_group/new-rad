$( document ).ready(function() {
  if ( $('.js--arrow-animotion').length ) {

    $('.js--arrow-animotion-link').click(function(event) {

      event.preventDefault();

      var id = $(this).attr('href');
      var target = $(id);

      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1800 );
    });

    $(window).on('scroll', function() {
      if ($(window).scrollTop() >= 20 ) {
        $('.js--arrow-animotion').addClass('scrolled');
      } else {
        $('.js--arrow-animotion').removeClass('scrolled');
      }
    });

  }

});
