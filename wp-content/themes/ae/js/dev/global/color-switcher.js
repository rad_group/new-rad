$( document ).ready(function() {
  //Il target è il riferimento della posizione in cui deve fare il controllo e/o l'elemento che cambia colore
  $('.black-bg-target').colorSwitcher({
    class: 'color-switcher', //classe degli elementi che contraddistinguono il cambio, senza il colore. Definirlo sugli elementi nel seguente modo: classe__colore (color-switcher__black)
    class_marginY: 'color-switcher__marginY', //classe che se settata tiene conto o no anche dei margini in altezza dell'elemento. Default: color-switcher__margin-no. ES: color-switcher__margin-yes, color-switcher__margin-no.
    class_marginX: 'color-switcher__marginX', //come sopra ma per la larghezza
    el: '', //se l'elemento che cambia colore è diverso dal trigger definirlo qui
    default: 'black', //colore di default
    default_marginY: false,
    default_marginX: false,
    left: 24, //fix left
    top: 32, //fix top
    colors:{'black' : '', 'white' : 'light-changing'} //definizione colore e classe associata
  });

});
