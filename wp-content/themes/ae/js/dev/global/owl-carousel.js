// Document Ready
$( document ).ready(function() {

  slider_content();
  add_classes();

  $(".js--composer-carousel--horizontal").owlCarousel({
    center: true,
    loop: true,
    items: 1,
    nav: false,
    dots: true,
    margin: 0,
    stagePadding: 50,
    navText: [],
    navSpeed: 1200,
    responsive : {
      640 : {
        stagePadding: 200,
      },
      1024 : {
        stagePadding: 300,
      },
      1440 : {
        stagePadding: 350,
      }
    }
  });

  $(".js--composer-carousel--vertical").owlCarousel({
    center: true,
    loop: true,
    items: 1,
    nav: false,
    dots: true,
    margin: 0,
    stagePadding: 50,
    navText: [],
    navSpeed: 1200,
    responsive : {
      640 : {
        stagePadding: 200,
      },
      1024 : {
        stagePadding: 300,
      },
      1440 : {
        stagePadding: 350,
      },
      1500 : {
        stagePadding: 450,
      },
      1600 : {
        stagePadding: 500,
      }
    }
  });

  $(".js--about-slider").owlCarousel({
    center: true,
    autoplay: true,
    autoplayTimeout: 6000,
    loop: true,
    items: 1,
    nav: false,
    dots: true,
    navText: [],
    stagePadding: 20,
    margin: 10,
    touchDrag: true,
    responsive : {
      640 : {
        items: 1,
        stagePadding: 20,
        margin: 10,
        touchDrag: false
      },
      1024 : {
        stagePadding: 40,
        margin: 20,
      },
    }
  });

  var current = $('.js--composer-carousel').find('.owl-item.center');
  current.prev().addClass('prev');
  current.next().addClass('next');


  // $owl.children().each( function( index ) {
  //   $(this).attr( 'data-position', index );
  // });
  //
  // $(document).on('click', '.owl-item', function() {
  //   $get_data = $(this).data('position');
  //   console.log($get_data);
  //   $owl.trigger('to.owl.carousel', $get_data );
  // });

}); //END Document Ready


function slider_content(){

  $('.prev').click(function(){
    console.log($(this));
    $('.owl-item').removeClass('prev');
    $('.owl-item').removeClass('next');
    $('.owl-item').removeClass('active');
    $(this).trigger('prev.owl.carousel');
    add_classes();
  });

  $('.next').click(function(){
    console.log('next');
    $('.owl-item').removeClass('prev');
    $('.owl-item').removeClass('next');
    $('.owl-item').removeClass('active');
    $(this).trigger('next.owl.carousel');
    add_classes();
  });
};

function add_classes() {
  var current = $('.js--composer-carousel').find('.owl-item.center');
  current.prev().addClass('prev');
  current.next().addClass('next');
};
