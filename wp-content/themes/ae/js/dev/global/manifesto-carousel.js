$( document ).ready(function() {

  var manifesto_carousel = $('.manifesto-wrap');
  manifesto_carousel.owlCarousel({
    items:1,
    dots: true,
    autoplay: true,
    loop: true,
    autoplayTimeout: 8000
  });

});
