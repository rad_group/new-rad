$(function () { // wait for document ready
  //Home slider
  if ( $('.js--slide-container').length ) {
    // init
    var controller = new ScrollMagic.Controller();

    // define movement of panels
    var wipeAnimation = new TimelineMax()
      // animate to second panel
      .to(".js--slide-container", 0.5, {z: -150})		// move back in 3D space
      .to(".js--slide-container", 1,   {x: "-25%"})	// move in to first panel
      .to(".js--slide-container", 0.5, {z: 0})				// move back to origin in 3D space
      // animate to third panel
      .to(".js--slide-container", 0.5, {z: -150, delay: 1})
      .to(".js--slide-container", 1,   {x: "-50%"})
      .to(".js--slide-container", 0.5, {z: 0})

    // create scene to pin and link animation
    new ScrollMagic.Scene({
        triggerElement: ".js--parent-slide-container",
        triggerHook: "onLeave",
        duration: "450%"
      })
      .setPin(".js--parent-slide-container")
      .setTween(wipeAnimation)
      //.addIndicators() // add indicators (requires plugin)
      .addTo(controller);
  }

  //Manifesto\ slider
  if ( $('.js--slide-manifesto-container').length ) {
    // init
    var controller = new ScrollMagic.Controller();

    // define movement of panels
    var wipeAnimation = new TimelineMax()
      // animate to second panel
      .to(".js--slide-manifesto-container", 1,   {x: "-10%"})	// move in to first panel
      // animate to third panel
      .to(".js--slide-manifesto-container", 1,   {x: "-20%"})
      // animate to fourth panel
      .to(".js--slide-manifesto-container", 1,   {x: "-30%"})
      // animate to fifth panel
      .to(".js--slide-manifesto-container", 1,   {x: "-40%"})
      // animate to sixth panel
      .to(".js--slide-manifesto-container", 1,   {x: "-50%"})
      // animate to seventh panel
      .to(".js--slide-manifesto-container", 1,   {x: "-60%"})
      // animate to eight panel
      .to(".js--slide-manifesto-container", 1,   {x: "-70%"})
      // animate to ninth panel
      .to(".js--slide-manifesto-container", 1,   {x: "-80%"})
      // animate to tenth panel
      .to(".js--slide-manifesto-container", 1,   {x: "-90%"})

    // create scene to pin and link animation
    new ScrollMagic.Scene({
        triggerElement: ".js--parent-slide-manifesto-container",
        triggerHook: "onLeave",
        duration: "300%"
      })
      .setPin(".js--parent-slide-manifesto-container")
      .setTween(wipeAnimation)
      //.addIndicators() // add indicators (requires plugin)
      .addTo(controller);
  }
});
