(function($) {

    $.fn.colorSwitcher = function(options) {

      // valori di default
      var config = {
        class: 'color-switcher',
        class_marginY: 'color-switcher__marginY',
        class_marginX: 'color-switcher__marginX',
        el: '',
        default: 'black',
        default_marginY: false,
        default_marginX: false,
        default: 'black',
        left: 0,
        top: 0,
        colors:{'black' : '', 'white' : 'element_white'}
      };

      if (options) $.extend(config, options);

      var $thisTrigger = $(this);
      var $thisClass = config.class;
      var $thisDefault = config.default;
      var $thisColors = config.colors;
      var $thisClassMarginH = config.class_marginY;
      var $thisClassMarginW = config.class_marginX;
      var $thisDefaultMarginW = config.default_marginX;
      var $thisDefaultMarginH = config.default_marginY;

      var getClassStartsWithColorSwitcher = function(t,n) {var r=$.grep(t.split(" "),function(t,r){return 0===t.indexOf(n)}).join();return r||!1};

      var getViewportOffsetColorSwitcher = function($e) {

        var $window = $(window),
          scrollLeft = $window.scrollLeft(),
          scrollTop = $window.scrollTop(),
          offset = $e.offset(),
          rect1 = { x1: scrollLeft, y1: scrollTop, x2: scrollLeft + $window.width(), y2: scrollTop + $window.outerHeight(true) },
          rect2 = { x1: offset.left, y1: offset.top, x2: offset.left + $e.width(), y2: offset.top + $e.outerHeight(true) };
        return {
          left: offset.left - scrollLeft,
          top: offset.top - scrollTop,
          insideViewport: rect1.x1 < rect2.x2 && rect1.x2 > rect2.x1 && rect1.y1 < rect2.y2 && rect1.y2 > rect2.y1
        };

      };

      var getElementsColorSwitcher = function() {
        var $x = $thisTrigger.position().top+config.top;
        var $y = $thisTrigger.position().left+config.left;

        var $triggerH = $thisTrigger.outerHeight(true);
        var $triggerH = parseInt($triggerH/2);
        var $x = $x+$triggerH;

        var $elements = $("body [class*='"+$thisClass+"']").map(function() {
            var $this = $(this);

            var $position = getViewportOffsetColorSwitcher($this);

            var $marginTop = 0;
            var $marginBottom = 0;
            var $marginLeft = 0;
            var $marginRight = 0;

            var $marginH = '';
            var $marginW = '';

            if($this.hasClass($thisClassMarginH+'-yes')){
              $marginH = true;
            }else if($this.hasClass($thisClassMarginH+'-no')){
              $marginH = false;
            }else{
               $marginH = $thisDefaultMarginH;
            }

            if($this.hasClass($thisClassMarginW+'-yes')){
              $marginW = true;
            }else if($this.hasClass($thisClassMarginW+'-no')){
              $marginW = false;
            }else{
               $marginW = $thisDefaultMarginW;
            }

            var tM = $this.css("margin").split(" ");
            var Margin = {
                Top: tM[0] != null ? parseInt(tM[0]) : 0,
                Right: tM[1] != null ? parseInt(tM[1]) : (tM[0] != null ? parseInt(tM[0]) : 0),
                Bottom: tM[2] != null ? parseInt(tM[2]) : (tM[0] != null ? parseInt(tM[0]) : 0),
                Left: tM[3] != null ? parseInt(tM[3]) : (tM[1] != null ? parseInt(tM[1]) : (tM[0] != null ? parseInt(tM[0]) : 0))
            };

            var $marginTop = Margin.Top;
            var $marginBottom = Margin.Bottom;
            var $marginLeft = Margin.Left;
            var $marginRight = Margin.Right;



            if($marginH == true){
              var $h = $this.outerHeight(true);
              var $t = $position.top-$marginTop;
            }else{
              var $h = $this.outerHeight();
              var $t = $position.top+$marginTop;
            }

            if($marginW == true){
              var $w = $this.outerWidth(true);
              var $l = $position.left-$marginLeft;
            }else{
              var $w = $this.outerWidth();
              var $l = $position.left+$marginLeft;
            }

            var $maxx = $l + $w;
            var $maxy = $t + $h;

            var $classes = $this[0].className;
            var $getClass = getClassStartsWithColorSwitcher($classes,'color-switcher');
            var $getColor = $getClass.split('color-switcher'+'__');
            var $color = $getColor[1];

            if((($x>=$l && $x<=$maxx) && ($x>= $t && $x<=$maxy)) && (($y>=$t && $y<=$maxy) && ($y>=$l && $y<=$maxx))){
              return $color;
            }
        });


        return $elements;

      };

      var actionColorSwitcher = function($c) {

        $element = $thisTrigger;

        if(config.el != ''){
          $element = $(config.el);
        }

        $.each( $thisColors, function( key, value ) {
          if(key != $c){
            $element.removeClass(value);
          }

        });

        $element.addClass($thisColors[$c]);

      };

      var initColorSwitcher = function() {
        $elementsView = getElementsColorSwitcher();

        $c = $thisDefault;

        if($elementsView.length>0){
          $c = $elementsView[0];
        }

        actionColorSwitcher($c);
      };

      $(window).on( 'resize', function(){
         initColorSwitcher();
      });

      $(document).on( 'scroll', function(){
         initColorSwitcher();
      });

      //initColorSwitcher();
    }

})(jQuery);
