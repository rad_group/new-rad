<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

get_header(); ?>

<div class="grid-container--large">
  <main id="main" class="site-main" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">

    <header class="blog-header row__column tab-12">
      <div class="grid-container--large">
        <h1 class="primary-title">
          <div class="glitch-black" data-text="Blog">
            <?php _e('Blog', 'ae'); ?>
          </div>
        </h1>
        <h2 class="secondary-title"><?php _e('Rad Magazine', 'ae'); ?></h2>
      </div>
    </header>
    <div class="row blog-row">
      <?php
      if ( have_posts() ) :
        $postNumber = 0;

        /* Start the Loop */
        while ( have_posts() ) : the_post(); ?>

        <?php
        if ( $postNumber <= 2 ) {
          $postNumber ++;
        } else {
          $postNumber = 0;
          $postNumber ++;
        }?>

          <div class="row__column tab-6 desk-4 <?php if ( $postNumber == 2 ) { echo 'upper-post';} ?>">
            <?php get_template_part( 'template-parts/content', 'post' ); ?>
          </div>

        <?php endwhile;

        the_posts_navigation();

      else :

        get_template_part( 'template-parts/content', 'none' );

      endif; ?>
    </div>
  </main><!-- #main -->
</div>

<?php get_footer(); ?>
