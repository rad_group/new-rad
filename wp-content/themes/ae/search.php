<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package framaework
 */

get_header(); ?>

<div class="grid-container--large">
  <div class="row">
    <div class="row__column tab-8">
      <main id="main" class="site-main" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/SearchResultsPage">

      <?php if ( have_posts() ) : ?>

        <header class="page-header">
          <h1 class="page-header__page-title">
            <?php printf( esc_html__( 'Search Results for: %s', 'ae' ), '<span>' . get_search_query() . '</span>' ); ?>
          </h1>
        </header><!-- .page-header -->

        <?php
        /* Start the Loop */
        while ( have_posts() ) : the_post();

          get_template_part( 'template-parts/content', 'search' );

        endwhile;

          the_posts_navigation();

      else :
        get_template_part( 'template-parts/content', 'none' );
      endif; ?>

      </main><!-- #main -->
    </div>

    <div class="row__column tab-4">
      <?php get_sidebar(); ?>
    </div>

  </div> <!-- .row -->
</div>

<?php get_footer(); ?>
