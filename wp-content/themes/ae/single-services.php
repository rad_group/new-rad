<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main">

  <?php
  while ( have_posts() ) : the_post();

    get_template_part( 'template-parts/content', 'page' );

    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
      comments_template();
    endif;

  endwhile; // End of the loop.
  ?>

  <div class="contact-form__flat">
    <div class="grid-container--small">
      <?php echo do_shortcode( '[contact-form-7 id="515" title="Service form 1"]' ) ?>
    </div>
  </div>

<?php $page_id = get_the_ID(); ?>

<section class="related-services">
  <div class="canvas-container">
    <canvas class="js--particles particles" data-color="<?php if( checkCookiesSet() == 'negative' ) { echo 'black'; } else { echo 'white'; } ?>" height="1280"></canvas>
  </div>
  <div class="grid-container p-relative">

    <h3 class="prefix-title"><?php _e('Tutti i nostri servizi'); ?></h3>

    <ul class="services-li">
      <?php
      $args = array( 'post_type' => 'services', 'posts_per_page' => 99 );
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post(); ?>

      <?php
      $current_id = $post->ID;

      if ( $current_id == $page_id ) {
        $current = 'services-li__single--current';
      } else {
        $current = 'services-li__single';
      }
      ?>

        <li class="<?php echo $current; ?>">
          <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
            <?php the_title(); ?>
          </a>
        </li>
      <?php endwhile; ?>
    </ul>
  </div>
</section>

</main><!-- #main -->
<?php get_footer(); ?>
