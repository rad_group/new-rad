<?php
/**
 * The template to display About.
 *
 * Template Name: About
 *
 * This is the template that displays About.
 *
 * @package framaework
 */
 get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

  <section class="full-screen js--rad-waypoint fade--in-up on-screen">

    <div class="overflow-hidden">
      <div class="canvas-container">
        <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
      </div>

      <div class="height-screen__2-3 color-switcher__black">
        <div class="grid-container--small vertical-align--tab">

          <header class="page__header">
            <hgroup class="page__hgroup">
              <h1 class="primary-title js--rad-waypoint fade--in-up on-screen">
                <div class="glitch-black" data-text="<?php the_title(); ?>">
                  <?php the_title(); ?>
                </div>
              </h1>
              <?php if( get_field('post_subtitle') ) : ?>
                <h2 class="secondary-title js--rad-waypoint fade--in-up on-screen"><?php echo get_field('post_subtitle'); ?></h2>
              <?php endif; ?>
            </hgroup>
          </header><!-- .page__header -->

          <div class="formatted-content last-p js--rad-waypoint fade--in-up on-screen">
            <?php echo get_field('about_intro_txt'); ?>
          </div>

        </div>
      </div>

      <div class="about-video__box js--rad-waypoint fade--in-up on-screen color-switcher__white">
        <div class="full-width-bg about-video js--bg-waypoint js--dark-bg">
          <div class="video-ui js--about-video-ui about-video-ui">
            <video class="about-video-ui__video lazy" preload="metadata" autoplay loop muted playsinline poster="<?php //bloginfo('template_directory'); ?>">
              <source data-src="<?php bloginfo('template_directory'); ?>/video/rad-motto.mp4" type="video/mp4">
              <!-- <source data-src="<?php //bloginfo('template_directory'); ?>/video/rad-video.webm" type="video/webm"> -->
            </video>
            <!-- <div class="about-video-ui__playpause vertical-align">
              <div class="about-video-ui__playpause--text vertical-align js--about-video">
                <?php //_e('Watch', 'ae'); ?>
              </div>
            </div> -->
          </div>

          <div class="arrow-animotion--white js--arrow-animotion">
            <a class="arrow-animotion__link js--arrow-animotion-link" href="#arrow-target">
              <div class="arrow-animotion__dynamic">
                <svg x="0px" y="0px" width="22px" height="29px">
                  <path d="M21.7,17.3c-0.4-0.4-1-0.4-1.4,0L12,25.6V1c0-0.6-0.4-1-1-1s-1,0.4-1,1v24.6l-8.3-8.3c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4
                  l10,10c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l10-10C22.1,18.3,22.1,17.7,21.7,17.3z"/>
                </svg>
              </div>
            </a>
          </div>
        </div>
      </div>

    </div>
  </section>

  <section class="about js--dark-bg bg-screen color-switcher__white" id="arrow-target">
    <div class="row--stuck columns-middle full-screen">
      <div class="row__column desk-5">
        <div class="about-content">
          <h3 class="about-content__title js--rad-waypoint fade--in-up" data-offset_delay="10"><?php _e('Our mission is: Communication', 'ae') ?></h3>
          <p class=" js--rad-waypoint fade--in-up" data-offset_delay="10"><?php _e('We aim to make people and companies communicate, bridging the gap between analog and digital experience. We create a unique and representative identity for your brand, in the same way we connect your management system to our interactive systems.', 'ae') ?></p>
          <p class=" js--rad-waypoint fade--in-up" data-offset_delay="10"><?php _e('From the physical flagship store to eCommerce online, we follow the entire art direction, branding and web development process, to promote your brand and your vision.', 'ae') ?></p>
        </div>
      </div>
      <div class="row__column desk-7 column-content">

        <?php
        $gallery = get_field('about_slider');
        $size = 'full';
        if( $gallery ){ ?>

          <div class="about-slider owl-carousel js--about-slider">
            <?php foreach($gallery as $slider) { ?>
              <img class="lazy" data-src="<?php echo $slider['url']; ?>" src="<?php echo $slider['url']; ?>" alt="Immagine slider">
            <?php }; ?>
          </div>

        <?php } ?>
      </div>
    </div>
  </section>

  <section class="full-screen--dark overflow-x js--dark-bg js--parent-slide-manifesto-container color-switcher__white">
    <div class="canvas-container">
      <canvas class="js--particles particles" data-color="<?php if( checkCookiesSet() == 'negative' ) { echo 'white'; } else { echo 'black'; } ?>" height="1280"></canvas>
    </div>
    <div class="header-manifesto">
      <div class="grid-container txt-center vertical-align">
        <h2 class="composer__txt-title js--rad-waypoint fade--in-up">
          <?php _e('Our Manifesto', 'ae'); ?>
        </h2>
        <div class="composer__txt-underscore js--rad-waypoint fade--in-up" data-offset_delay="20"></div>
      </div>
    </div>

    <div class="slide-manifesto-container js--slide-manifesto-container">
      <?php
      $args = array( 'post_type' => 'commandments', 'posts_per_page' => -1 );
      $loop = new WP_Query( $args );
      $counter = 0;
      while ( $loop->have_posts() ) : $loop->the_post();
        $counter++
      ?>
      <article class="slide-manifesto-container__panel single-law">
        <div class="slide-container__panel-link">
          <div class="grid-container--large">
            <div class="row">
              <div class="row__column mobile-3">
                <div class="single-law__h">
                  <div class="single-law__number">
                    <div class="glitch" data-text="<?php echo $counter; ?>">
                      <?php echo $counter; ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row__column mobile-9">
                <div class="single-law__h">
                  <div class="single-law__content <?php if ( $counter != 0 ) { echo 'hidden-law'; } ?>">
                    <?php the_content(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </article>

      <?php endwhile; ?>
    </div>
    <?php wp_reset_postdata(); ?>

  </section>

  <section class="our-clients bg-screen color-switcher__black">
    <div class="grid-container--large">
      <?php
      $args = array( 'post_type' => 'clients', 'posts_per_page' => -1 );
      $loop = new WP_Query( $args ); ?>
      <div class="row">
        <div class="row__column tab-3 desk-3">
          <h3 class="our-clients__title"><?php _e('_Our Clients', 'ae'); ?></h3>
        </div>
        <div class="row__column tab-9 desk-9">
          <div class="row--stuck">
            <?php while ( $loop->have_posts() ) : $loop->the_post();
              $post = $loop->post;
            ?>
              <div class="row__column mobile-6 desk-3">
                <h4 class="our-clients__brand js--rad-waypoint fade--in-up" data-offset_delay="10">
                  <?php echo $post->post_title; ?>
                </h4>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
      </div>
    </div>
  </section>


</main><!-- #main -->
 <?php get_footer(); ?>
