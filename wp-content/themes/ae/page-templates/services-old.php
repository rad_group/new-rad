<?php
/**
 * The template to display Services.
 *
 * Template Name: Services old
 *
 * This is the template that displays Services.
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
  <?php
  while ( have_posts() ) : the_post();

    get_template_part( 'template-parts/content', 'page' );

    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
      comments_template();
    endif;

  endwhile; // End of the loop.
  ?>

  <div class="highlight-services">
    <div class="row">
      <?php
      $args = array(
        'post_type' => 'services',
        'posts_per_page' => 99,
        'tax_query' => array(
          array(
            'taxonomy' => 'services_category',
            'field' => 'slug',
            'terms' => 'highlight',
            'operator' => 'IN'
          )
        ),
      );
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <div class="row__column tab-4">
        <article class="single-service--highlight">
          <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
            <h2 class="single-service__title"><?php the_title(); ?></h2>
            <div class="single-service__content">
              <?php the_excerpt(); ?>
            </div>
            <span class="rad-btn--white">
              <?php _e('Approfondisci', 'ae'); ?>
            </span>
          </a>
        </article>
      </div>
      <?php endwhile; ?>
      <?php wp_reset_query(); ?>
    </div>
  </div>

  <section class="services-list">
    <div class="grid-container--large">
      <div class="row js--grid">
        <?php
        $args = array(
          'post_type' => 'services',
          'posts_per_page' => 99,
          'tax_query' => array(
            array(
              'taxonomy' => 'services_category',
              'field' => 'slug',
              'terms' => 'highlight',
              'operator' => 'NOT IN'
            )
          ),
        );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <div class="row__column desk-4 tab-6 js--grid-item">
          <article class="single-service">
            <a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>">
              <h2 class="single-service__title"><?php the_title(); ?></h2>
              <div class="single-service__content">
                <?php the_excerpt(); ?>
              </div>
              <!-- <span class="rad-link">
                <?php _e('Approfondisci', 'ae'); ?>
                <i class="iconae regular ae--arrow-right"></i>
              </span> -->
            </a>
          </article>
        </div>
        <?php endwhile; ?>
      </div>
    </div>
  </section>

  <?php get_template_part( 'template-parts/content', 'call_to_action' ); ?>
</main><!-- #main -->
<?php get_footer(); ?>
