<?php
/**
 * The template to display Work with us.
 *
 * Template Name: Work with us
 *
 * This is the template that displays About.
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

  <?php while ( have_posts() ) : the_post(); ?>

    <div class="overflow-hidden color-switcher__black">
      <div class="canvas-container">
        <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
      </div>

      <div class="grid-container--small">

        <header class="page__header">
          <hgroup class="page__hgroup">
            <h1 class="primary-title">
              <div class="glitch-black" data-text="<?php the_title(); ?>">
                <?php the_title(); ?>
              </div>
            </h1>
            <?php if( get_field('post_subtitle') ) : ?>
              <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
            <?php endif; ?>
          </hgroup>
        </header><!-- .page__header -->

        <div class="page__intro formatted-content">
          <?php echo get_field('about_intro_txt'); ?>
          <div class="jobs-privacy"><?php _e('Our calls for application are opened to any gender and to people of any age and nationality in accordance with laws 903/77 and 125/91 and legislative decrees 215/03 and 216/03', 'ae'); ?></div>
        </div>

      </div>
    </div>

  <?php endwhile; ?>

  <?php wp_reset_query(); ?>

  <div class="works-count js--rad-waypoint fade--in-up on-screen color-switcher__white bg-screen">
    <div class="grid-container">
      <?php
      $args = array( 'post_type' => 'jobs', 'posts_per_page' => 999 );
      $loop = new WP_Query( $args );

      if ( $loop->have_posts() ): ?>
        <h3 class="secondary-title jobs-title"><?php _e('Open positions', 'ae'); ?>:</h3>
        <ul class="accordion js--accordion">
          <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <li class="accordion__single js--accordion__single">
              <div class="accordion__single-header js--accordion__single-header">
                <?php the_title(); ?>
                <div class="jobs-position">
                  <p><?php _e('Position:', 'ae'); ?></p>
                  <?php if( get_field('position') == true ): ?>
                    <span><?php _e('Open', 'ae'); ?></span>
                  <?php else: ?>
                    <span><?php _e('Closed', 'ae'); ?></span>
                  <?php endif; ?>
                </div>
                <div class="accordion__single-header-ui">
                  <i class="iconae regular ae--arrow-down"></i>
                </div>
              </div>
              <div class="accordion__single-content js--accordion__single-content">
                <div class="formatted-content jobs-intro">
                  <?php the_content(); ?>
                </div>
                <div class="job-skills">
                  <div class="row">
                    <div class="row__column tab-6">
                      <div class="formatted-content">
                        <?php if( have_rows('jobs_list_foundamentals') ): ?>

                          <h4 class="jobs-list__title"><?php _e('Basic requirements:', 'ae'); ?></h4>

                          <ul class="jobs-list">
                          <?php while ( have_rows('jobs_list_foundamentals') ) : the_row(); ?>

                            <li class="jobs-list__single"><?php the_sub_field('jobs_list_foundamentals_txt'); ?></li>

                          <?php endwhile; ?>
                          </ul>

                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="row__column tab-6">
                      <div class="formatted-content">
                        <?php if( have_rows('jobs_list_ottimali') ): ?>

                          <h4 class="jobs-list__title"><?php _e('Optimal requirements:', 'ae'); ?></h4>

                            <ul class="jobs-list">
                          <?php while ( have_rows('jobs_list_ottimali') ) : the_row(); ?>

                            <li class="jobs-list__single"><?php the_sub_field('jobs_list_ottimali_txt'); ?></li>

                          <?php endwhile; ?>
                          </ul>

                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          <?php endwhile; ?>

          <?php wp_reset_query(); ?>

        </ul>
      <?php else: ?>
          <div class="grid-container--small txt-center">
            <b><?php _e('In questo momento non sono presenti delle richieste specifiche.', 'ae'); ?></b>
          </div>
      <?php endif; ?>

    </div>
  </div>

  <section class="jobs-form contact-form js--rad-waypoint fade--in-up on-screen color-switcher__black">
    <div class="grid-container">

      <?php
      function cf7_dynamic_select_do_example1($choices, $args=array()) {

        global $loop;

        $choices = array(
          '-- Seleziona una posizione aperta --' => ''
        );

        while ( $loop->have_posts() ) : $loop->the_post();
          $post = $loop->post;
          if( get_field('position', $post->ID ) == true ):
            $choices[get_the_title()] = get_the_title();
          endif;
        endwhile;

        return $choices;
      }

      if ( $loop->have_posts() ):
        add_filter('wpcf7_dynamic_select_example1', 'cf7_dynamic_select_do_example1', 10, 2);
      endif;
      ?>

      <div class="contact-form__flat jobs-contact-form">
        <div class="formatted-content">
          <h2><?php _e('_Send us your request', 'ae'); ?></h2>
        </div>
        <?php if(ICL_LANGUAGE_CODE=='en') : ?>
          <?php echo do_shortcode('[contact-form-7 id="6824" title="Jobs Form_ENG"]'); ?>
        <?php else : ?>
          <?php echo do_shortcode('[contact-form-7 id="5299" title="Jobs Form"]'); ?>
        <?php endif; ?>
      </div>
    </div>
  </section>

</main><!-- #main -->

<?php get_footer(); ?>
