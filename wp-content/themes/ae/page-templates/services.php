<?php
/**
 * The template to display Services.
 *
 * Template Name: Services
 *
 * This is the template that displays Services.
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
  <div class="overflow-hidden color-switcher__black">
    <div class="canvas-container">
      <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
    </div>

    <div class="grid-container--small">

      <header class="page__header">
        <hgroup class="page__hgroup">
          <h1 class="primary-title">
            <div class="glitch-black" data-text="<?php the_title(); ?>">
              <?php the_title(); ?>
            </div>
          </h1>
          <?php if( get_field('post_subtitle') ) : ?>
            <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
          <?php endif; ?>
        </hgroup>
      </header><!-- .page__header -->

      <div class="page__intro formatted-content">
        <?php echo get_field('about_intro_txt'); ?>
      </div>

    </div>
    <div class="arrow-animotion grid-container--large js--arrow-animotion txt-center">
      <a class="arrow-animotion__link js--arrow-animotion-link" href="#start-post">
        <div class="arrow-animotion__dynamic">
          <svg x="0px" y="0px" width="22px" height="29px">
            <path d="M21.7,17.3c-0.4-0.4-1-0.4-1.4,0L12,25.6V1c0-0.6-0.4-1-1-1s-1,0.4-1,1v24.6l-8.3-8.3c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4
            l10,10c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l10-10C22.1,18.3,22.1,17.7,21.7,17.3z"/>
          </svg>
        </div>
      </a>
    </div>
  </div>

  <div class="grid-container--small">
    <div class="formatted-content">

      <div class="indent-2 page__content">
        <?php the_content(); ?>
      </div><!-- .page__content -->

    </div>
  </div>
  <!-- <div id="test-pippo" style="height:10px;"></div> -->

  <section class="pure-black-bg js--bg-waypoint color-switcher__white" id="start-post">
    <div class="service-folder-wrap js--rad-waypoint fade--in-up">
      <div class="grid-container--huge">
        <div class="row">
          <?php
          $args = array(
            'post_type' => 'servicefolder',
            'posts_per_page' => -1
          );
          $loop = new WP_Query( $args );
          while ( $loop->have_posts() ) : $loop->the_post(); ?>

            <div class="row__column tab-6">
              <article class="service-folder">
                <img class="service-folder__icn js--rad-waypoint fade--in-up" src="<?php the_field('sub_services_icn'); ?>" alt="<?php the_title(); ?> Icon">
                <hgroup>
                  <h2 class="service-folder__title js--rad-waypoint fade--in-up" data-offset_delay="10"><?php the_title(); ?></h2>
                  <h3 class="service-folder__desc js--rad-waypoint fade--in-up" data-offset_delay="20"><?php the_field('sub_services_subttitle'); ?></h3>
                </hgroup>
                <div class="composer__txt-underscore js--rad-waypoint fade--in-up" data-offset_delay="30"></div>
                <?php if( have_rows('sub_services') ) : ?>

                  <ul class="service-folder__ul">
                    <?php $i = 1; ?>
                    <?php while ( have_rows('sub_services') ) : the_row(); ?>

                      <li class="service-folder__li js--rad-waypoint fade--in-up" data-offset_delay="10">
                        <?php the_sub_field('sub_services_title'); ?>
                      </li>
                      <?php $i ++; ?>
                    <?php endwhile; ?>
                  </ul>

                <?php endif; ?>
              </article>
            </div>

          <?php endwhile; ?>

        </div>
      </div>
    </div>
  </section>

  <?php //get_template_part( 'template-parts/content', 'dna' ); ?>

  <?php get_template_part( 'template-parts/content', 'rad-archive' ); ?>

</main><!-- #main -->
<?php get_footer(); ?>
