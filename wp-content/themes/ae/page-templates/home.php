<?php
/**
 * The template to display Home.
 *
 * Template Name: Home
 *
 * This is the template that displays Home.
 *
 * @package framaework
 */

get_header(); ?>

<section class="home-hero full-screen">
  <div class="canvas-container">
    <canvas class="js--particles particles" data-color="<?php if( checkCookiesSet() == 'negative' ) { echo 'black'; } else { echo 'white'; } ?>" height="1280"></canvas>
  </div>
  <div class="grid-container--large vertical-align">
    <hgroup class="home-hero__hgroup">
      <h2 class="prefix-title">We RAD <span class="prefix-title__opacity">(all together)</span></h3>
      <h3 class="huge-title">
        <div class="glitch-black" data-text="<?php _e('We dare because we care.', 'ae'); ?>">
          <?php _e('We dare because we care.', 'ae'); ?>
        </div>
      </h3>
      <h1 class="secondary-title">
        <span class="span-br"><?php _e('L’Agenzia di comunicazione a Firenze', 'ae'); ?></span>
      </h1>

    </hgroup>

  </div>

  <div class="arrow-animotion grid-container--large js--arrow-animotion">
    <a class="arrow-animotion__link js--arrow-animotion-link" href="#about-home">
      <div class="arrow-animotion__dynamic">
        <svg x="0px" y="0px" width="22px" height="29px">
          <path d="M21.7,17.3c-0.4-0.4-1-0.4-1.4,0L12,25.6V1c0-0.6-0.4-1-1-1s-1,0.4-1,1v24.6l-8.3-8.3c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4
          l10,10c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l10-10C22.1,18.3,22.1,17.7,21.7,17.3z"/>
        </svg>
      </div>
    </a>
  </div>

</section>

  <main id="main" class="site-main bg-screen color-switcher__black" role="main" role="main" itemprop="mainContentOfPage">
    <section class="home-case-histories full-screen js--parent-slide-container" id="about-home">
      <div class="height-screen__1-3 little">
        <div class="vertical-align grid-container--large">
          <div class="row">
            <div class="row__column tab-4">
              <h2 class="composer__txt-title js--rad-waypoint fade--in-up"><?php _e('Our case history', 'ae'); ?></h2>
            </div>
            <div class="row__column tab-8">
              <div class="composer__txt-only--content formatted-content js--rad-waypoint fade--in-up">
                <p><?php _e("<b>WE RAD</b> è un’agenzia di comunicazione con sede a Firenze, specializzata nell'ideazione di strategie di branding per medie e grandi aziende. Crediamo nella <b>continua evoluzione</b> che ci consente di instaurare un legame bidirezionale e duraturo con i nostri Clienti.", 'ae'); ?></p>
              </div>
              <a class="rad-btn animsition-link js--rad-waypoint fade--in-up" href="<?php echo get_permalink('5628'); ?>" title="<?php _e('Vedi tutti i case history', 'ae'); ?>">
                <?php _e('Vedi tutti i casi studio', 'ae'); ?>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="height-screen__2-3 big">
        <?php $post_objects = get_field('home_works_selection'); ?>
        <?php if( $post_objects ): ?>
          <div class="slide-container js--slide-container">
            <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
              <?php setup_postdata($post); ?>
              <article class="slide-container__panel">
                <a class="animsition-link slide-container__panel-link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                  <img class="slide-container__panel-img lazy" data-src="<?php echo get_the_post_thumbnail_url($post, 'large'); ?>" src="<?php echo get_the_post_thumbnail_url($post, 'large'); ?>" title="<?php _e('Lavoro di ', 'ae') ?> <?php the_title(); ?>">
                  <div class="slide-container__panel-overlay">
                    <div class="slide-container__panel-overlay-txt">
                      <h2 class="slide-container__panel-overlay-title">
                        <?php the_title(); ?>
                      </h2>
                      <?php
                      $post_ID = $post->ID;
                      if(have_rows('works_services', $post->ID)): ?>
                        <ul class="slide-container__panel-services-list">
                          <?php while(have_rows('works_services', $post->ID)): the_row(); ?>
                            <li class="slide-container__panel-services-list-item">#<?php echo get_sub_field('works_single_service', $post->ID); ?></li>
                          <?php endwhile; ?>
                        </ul>
                      <?php endif; ?>
                    </div>
                  </div>
                </a>
              </article>
            <?php endforeach; ?>
          </div>
          <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      </div>
    </section>

    <?php get_template_part( 'template-parts/content', 'services-list' ); ?>

  </main><!-- #main -->
</div>
<?php get_footer(); ?>
