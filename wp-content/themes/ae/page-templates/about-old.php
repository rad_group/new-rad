<?php
/**
 * The template to display About.
 *
 * Template Name: About
 *
 * This is the template that displays About.
 *
 * @package framaework
 */
 get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
     <?php while ( have_posts() ) : the_post(); ?>

      <section class="full-screen">

        <div class="overflow-hidden">
          <div class="canvas-container">
            <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
          </div>

          <div class="height-screen__2-3">
            <div class="grid-container--small vertical-align--tab">

              <header class="page__header">
                <hgroup class="page__hgroup">
                  <h1 class="primary-title">
                    <div class="glitch-black" data-text="<?php the_title(); ?>">
                      <?php the_title(); ?>
                    </div>
                  </h1>
                  <?php if( get_field('post_subtitle') ) : ?>
                    <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
                  <?php endif; ?>
                </hgroup>
              </header><!-- .page__header -->

              <div class="formatted-content last-p">
                <?php echo get_field('about_intro_txt'); ?>
              </div>

            </div>
          </div>

          <div class="height-screen__1-3">
            <div class="full-width-bg js--bg-waypoint js--dark-bg">
              <div class="grid-container vertical-align">
                <img src="<?php bloginfo('template_url'); ?>/img/about.gif" alt="GIF dei soci di RAD - Agenzia di Comunicazione">
              </div>

              <div class="arrow-animotion--white js--arrow-animotion">
                <a class="arrow-animotion__link js--arrow-animotion-link" href="#arrow-target">
                  <div class="arrow-animotion__dynamic">
                    <svg x="0px" y="0px" width="22px" height="29px">
                      <path d="M21.7,17.3c-0.4-0.4-1-0.4-1.4,0L12,25.6V1c0-0.6-0.4-1-1-1s-1,0.4-1,1v24.6l-8.3-8.3c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4
                      l10,10c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l10-10C22.1,18.3,22.1,17.7,21.7,17.3z"/>
                    </svg>
                  </div>
                </a>
              </div>

            </div>
          </div>

        </div>

      </section>

      <section class="full-screen" id="arrow-target">
        <div class="row--stuck">
          <div class="row__column tab-4">
            <div class="full-height background-img--about">
            </div>
          </div>
          <div class="row__column tab-8 column-content">

            <div class="composer__txt-only-wrap vertical-align grid-container">
              <div class="composer__txt-only">
                <h2 class="composer__txt-title js--rad-waypoint fade--in-up" data-offset_delay="10">La nostra mission è comunicare</h2>
                <div class="composer__txt-underscore js--rad-waypoint fade--in-up" data-offset_delay="20"></div>
                <div class="composer__txt-only--content formatted-content js--rad-waypoint fade--in-up" data-offset_delay="30">
                  <p>Miriamo a far comunicare persone e aziende, colmando il divario tra l’esperienza analogica e quella digitale. Creiamo un’identità unica e rappresentativa per il tuo brand, allo stesso modo in cui connettiamo il tuo gestionale ai nostri sistemi interattivi.</p>
                  <p>Dal flagship store fisico all’eCommerce online, seguiamo l’intero processo di art direction, branding e sviluppo web, per promuovere il tuo brand e la tua vision.</p>
                </div>
                <a class="rad-btn js--rad-waypoint fade--in-up" data-offset_delay="40" href="<?php echo get_permalink(491); ?>" title="<?php _e('Scopri lo studio', 'ae'); ?>">
                  <?php _e('Scopri lo studio', 'ae'); ?>
                </a>
              </div>
            </div>

          </div>
        </div>
      </section>

      <section class="full-screen--dark js--bg-waypoint js--dark-bg">
        <div class="row--stuck">
          <div class="row__column desk-8 column-content">
            <div class="full-height">
              <div class="services-cards vertical-align grid-container">
                <div class="row">
                  <div class="row__column tab-3">
                    <article class="card-service art js--rad-waypoint fade--in-up" data-offset_delay="0">
                      <h2 class="card-service__title"><?php _e('Art Direction', 'ae'); ?></h2>
                      <img class="card-service__icn" src="<?php bloginfo('template_directory'); ?>/img/services-icns/rad-artdirection.svg" alt="<?php _e('Art Direction', 'ae'); ?>">
                    </article>
                  </div>
                  <div class="row__column tab-3">
                    <article class="card-service strategy js--rad-waypoint fade--in-up" data-offset_delay="20">
                      <h2 class="card-service__title"><?php _e('Strategy', 'ae'); ?></h2>
                      <img class="card-service__icn" src="<?php bloginfo('template_directory'); ?>/img/services-icns/rad-strategy.svg" alt="<?php _e('Strategy', 'ae'); ?>">
                    </article>
                  </div>
                  <div class="row__column tab-3">
                    <article class="card-service digital js--rad-waypoint fade--in-up" data-offset_delay="10">
                      <h2 class="card-service__title"><?php _e('Digital & Web', 'ae'); ?></h2>
                      <img class="card-service__icn" src="<?php bloginfo('template_directory'); ?>/img/services-icns/rad-web.svg" alt="<?php _e('Digital & Web', 'ae'); ?>">
                    </article>
                  </div>
                  <div class="row__column tab-3">
                    <article class="card-service tech js--rad-waypoint fade--in-up" data-offset_delay="30">
                      <h2 class="card-service__title"><?php _e('Technologies', 'ae'); ?></h2>
                      <img class="card-service__icn" src="<?php bloginfo('template_directory'); ?>/img/services-icns/rad-technologies.svg" alt="<?php _e('Technologies', 'ae'); ?>">
                    </article>
                  </div>
                  <div class="row__column tab-3 tab-push-3 js--rad-waypoint fade--in-up" data-offset_delay="10">
                    <article class="card-service branding">
                      <h2 class="card-service__title"><?php _e('Branding', 'ae'); ?></h2>
                      <img class="card-service__icn" src="<?php bloginfo('template_directory'); ?>/img/services-icns/rad-branding.svg" alt="<?php _e('Branding', 'ae'); ?>">
                    </article>
                  </div>
                  <div class="row__column tab-3 tab-push-3 js--rad-waypoint fade--in-up" data-offset_delay="20">
                    <article class="card-service video">
                      <h2 class="card-service__title"><?php _e('Photo & Video', 'ae'); ?></h2>
                      <img class="card-service__icn" src="<?php bloginfo('template_directory'); ?>/img/services-icns/rad-production.svg" alt="<?php _e('Photo & Video', 'ae'); ?>">
                    </article>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row__column desk-4 column-content">
            <div class="full-height full-txt-services">
              <div class="composer__txt-only-wrap vertical-align grid-container--small">
                <div class="composer__txt-only">
                  <h2 class="composer__txt-title js--rad-waypoint fade--in-up" data-offset_delay="10"><?php _e('Servizi', 'ae'); ?></h2>
                  <div class="composer__txt-underscore js--rad-waypoint fade--in-up" data-offset_delay="20"></div>
                  <div class="composer__txt-only--content formatted-content js--rad-waypoint fade--in-up" data-offset_delay="30">
                    <p>Facciamo comunicare brand e aziende, trovando il giusto tone of voice e realizzando una brand identity in grado di essere declinata tramite un’art direction basata su analisi ben precise.<br>Sviluppiamo siti web, prendendoci cura della loro usabilità e delle loro prestazioni e facciamo comunicare i nostri servizi con qualsiasi gestionale.</p>
                  </div>
                  <a class="rad-btn js--rad-waypoint fade--in-up" data-offset_delay="40" href="<?php echo get_permalink(20); ?>" title="<?php _e('I Founders di RAD', 'ae'); ?>">
                    <?php _e('Scopri i servizi', 'ae'); ?>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="full-screen--dark overflow-x js--bg-waypoint js--dark-bg">
        <div class="canvas-container">
          <canvas class="js--particles particles" data-color="<?php if( checkCookiesSet() == 'negative' ) { echo 'white'; } else { echo 'black'; } ?>" height="1280"></canvas>
        </div>
        <div class="header-manifesto">
          <div class="grid-container txt-center vertical-align">
            <h2 class="composer__txt-title js--rad-waypoint fade--in-up">
              <?php _e('Our Manifesto', 'ae'); ?>
            </h2>
            <div class="composer__txt-underscore js--rad-waypoint fade--in-up" data-offset_delay="20"></div>
          </div>
        </div>

        <div class="manifesto-wrap owl-carousel js--rad-waypoint">
          <?php
          $args = array( 'post_type' => 'commandments', 'posts_per_page' => 99 );
          $loop = new WP_Query( $args );
          $counter = 0;
          while ( $loop->have_posts() ) : $loop->the_post();
            $counter++
          ?>
            <article class="single-law">
              <div class="grid-container--large">
                <div class="row">
                  <div class="row__column tab-3">
                    <div class="single-law__h">
                      <div class="single-law__number">
                        <div class="glitch" data-text="<?php echo $counter; ?>">
                          <?php echo $counter; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row__column tab-9">
                    <div class="single-law__h">
                      <div class="single-law__content <?php if ( $counter != 0 ) { echo 'hidden-law'; } ?>">
                        <?php the_content(); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          <?php endwhile; ?>
        </div>

      </section>

    <?php endwhile; ?>
</main><!-- #main -->
 <?php get_footer(); ?>
