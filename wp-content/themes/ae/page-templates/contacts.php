<?php
/**
 * The template to display Contacts.
 *
 * Template Name: Contacts
 *
 * This is the template that displays Contacts.
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

  <section class="contacts js--rad-waypoint fade--in-up on-screen bg-screen color-switcher__black">
    <div class="contacts__head">

    </div>
    <div class="overflow-hidden">
      <div class="canvas-container">
      </div>
      <div class="grid-container--large js--rad-waypoint fade--in-up on-screen">
        <div class="row">

          <div class="row__column tab-6 txt-center">
            <div class="contacts__single">
              <h4><?php _e('Mail', 'ae'); ?></h4>
              <a href="mailto:hello@we-rad.com" title="Inviaci una mail" class="contacts__element">hello@we-rad.com</a>
            </div>
          </div>

          <div class="row__column tab-6 txt-center">
            <div class="contacts__single">
              <h4><?php _e('Office', 'ae'); ?></h4>
              <a href="tel:+39 055 0603050" title="Contattaci" class="contacts__element">+39 055 0603050</a>
            </div>
          </div>

        </div>

      </div>
    </div>
  </section>

  <section class="contact-form js--rad-waypoint fade--in-up on-screen bg-screen color-switcher__black">
    <div class="grid-container--large">
      <div class="row">
        <div class="row__column tab-3">
          <div class="contact-form__scaffolding">
            <div class="contact-form__scaffolding-box">
              <h2 class="contact-form__title">We Rad Headquarters</h2>
              <address><a href="https://goo.gl/maps/7P1FtLHhPT62">Borgo degli Albizi 14</a><br>50122 <?php _e('Florence (Italy)', 'ae'); ?></address>
            </div>
            <div class="contact-form__scaffolding-box">
              <ul class="contacts-list">
                <li><?php _e('Phone: ', 'ae'); ?><a href="tel:+39 055 0603050" title="Numero di telefono RAD">+39 055 0603050</a></li>
                <li><?php _e('Email: ', 'ae'); ?> <a href="mailto:hello@we-rad.com">hello@we-rad.com</a></li>
              </ul>
            </div>
            <div class="contact-form__scaffolding-box">
              <div class="contact-form__iva"><?php _e('VAT IT ', 'ae'); ?>06892240489</div>
            </div>
            <div class="contact-form__scaffolding-box">
              <ul class="contacts-list--social hidden--on-eq-mobile">
                <li>
                  <a href="https://www.instagram.com/we_rad/" class="social__link" target="_blank" title="Instagram">
                    Instagram
                  </a>
                </li>
                <li>
                  <a href="https://www.facebook.com/weradgroup/" class="social__link" target="_blank" title="Facebbok">
                    Facebook
                  </a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/company/rad_group" class="social__link" target="_blank" title="Linkedin">
                    Linkedin
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row__column tab-9">
          <div class="contact-form__flat">
            <?php if(ICL_LANGUAGE_CODE=='en') : ?>
              <?php echo do_shortcode('[contact-form-7 id="6823" title="FLAT Contact Form_new_ENG"]'); ?>
            <?php else : ?>
              <?php echo do_shortcode('[contact-form-7 id="6064" title="FLAT Contact Form_new"]'); ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php get_template_part( 'template-parts/content', 'rad-archive' ); ?>

</main><!-- #main -->

<?php get_footer(); ?>
