<?php
/**
 * The template to display Works.
 *
 * Template Name: Works
 *
 * This is the template that displays Works.
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
  <?php
  while ( have_posts() ) : the_post();

    get_template_part( 'template-parts/content', 'page' );

  endwhile; // End of the loop.
  ?>

  <?php wp_reset_query(); ?>

  <section class="works-list">
    <div class="row--stuck">
      <?php
      $args = array( 'post_type' => 'works', 'posts_per_page' => 99 );
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post(); ?>

      <div class="row__column tab-6">
        <article class="single-thumb-work__single">
          <a class="single-thumb-work__single-link animsition-link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <img class="single-thumb-work__single-img lazy js--rad-waypoint fade--in-up on-screen" data-src="<?php echo get_the_post_thumbnail_url($post, 'large'); ?>" src="<?php echo get_the_post_thumbnail_url($post, 'large'); ?>" title="<?php _e('Lavoro di ', 'ae') ?> <?php the_title(); ?>">
            <div class="single-thumb-work__overlay">
              <div class="single-thumb-work__overlay-txt">
                <h2 class="single-thumb-work__overlay-title">
                  <?php the_title(); ?>
                </h2>
                <?php if( have_rows('works_services') ): ?>
                  <ul class="single-thumb-work__services">
                  <?php while ( have_rows('works_services') ) : the_row(); ?>
                      <?php setup_postdata($post); ?>
                      <li class="single-thumb-work__single-service">
                        <span class="single-thumb-work__single-service-link">
                          <span>#</span><?php echo get_sub_field('works_single_service'); ?>
                        </span>
                      </li>
                  <?php endwhile; ?>
                  </ul>
                  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                <?php endif; ?>
              </div>
            </div>
          </a>
        </article>
      </div>

      <?php endwhile; ?>
    </div>
  </section>
</main><!-- #main -->
<?php get_footer(); ?>
