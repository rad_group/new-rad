<?php
/**
 * The template to display Manifesto.
 *
 * Template Name: Manifesto
 *
 * This is the template that displays Manifesto.
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
  <?php while ( have_posts() ) : the_post(); ?>

    <div class="overflow-hidden">
      <div class="canvas-container">
        <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
      </div>

      <div class="grid-container--small">

        <header class="page__header">
          <hgroup class="page__hgroup">
            <?php the_title( '<h1 class="primary-title">', '</h1>' ); ?>
            <?php if( get_field('post_subtitle') ) : ?>
              <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
            <?php endif; ?>
          </hgroup>
        </header><!-- .page__header -->

        <div class="page__intro formatted-content">
          <?php echo get_field('about_intro_txt'); ?>
        </div>

      </div>
    </div>

  <?php endwhile; ?>

    <div class="grid-container--large">

    <section class="commandments">
      <div class="grid-container--small">
        <h2 class="prefix-title"><?php _e('Rad Comandamenti'); ?></h2>
        <h3 class="primary-title"><?php _e('I nostri principi'); ?></h3>

        <div class="laws">
          <div class="row">
            <div class="row__column mobile-3 tab-2 no-relative">
              <div class="js--laws-number laws__number">
                1
              </div>
            </div>

            <div class="row__column mobile-9 tab-10">
              <ul class="js--laws__list laws__list">
                <?php
                $args = array( 'post_type' => 'commandments', 'posts_per_page' => 99 );
                $loop = new WP_Query( $args );
                $counter = 0;
                while ( $loop->have_posts() ) : $loop->the_post();
                  $counter++
                ?>
                  <li class="js--law-trigger laws__single" data-law="<?php echo $counter; ?>">
                    <?php the_content(); ?>
                  </li>
                <?php endwhile; ?>
              </ul>
            </div>
          </div>
        </div>

      </div>

    </section>

    <?php while ( have_posts() ) : the_post(); ?>

    <div class="grid-container--small">
      <h2 class="primary-title"><?php _e('La nostra etica'); ?></h2>
      <div class="formatted-content">

        <div class="indent-2 page__content">
          <?php the_content(); ?>
        </div><!-- .page__content -->

      </div>
    </div>

    <?php endwhile; // End of the loop.
    ?>
  </div>
  <div class="button-cta txt-center">
    <a class="rad-btn" href="<?php echo get_permalink(20); ?>" title="<?php _e('Scopri i nostri servizi'); ?>">
      <?php _e('Scopri i nostri servizi'); ?>
    </a>
  </div>
  <?php
  while ( have_posts() ) : the_post();

    get_template_part( 'template-parts/content', 'call_to_action' );

    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
      comments_template();
    endif;

  endwhile; // End of the loop.
  ?>

</main><!-- #main -->

  </div> <!-- .row -->
<?php get_footer(); ?>
