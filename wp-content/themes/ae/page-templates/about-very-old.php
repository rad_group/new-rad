<?php
/**
 * The template to display About.
 *
 * Template Name: About-old
 *
 * This is the template that displays About.
 *
 * @package framaework
 */
 get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
     <?php while ( have_posts() ) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class('page'); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">


        <div class="overflow-hidden">
          <div class="canvas-container">
            <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
          </div>

          <div class="grid-container--small">

            <header class="page__header">
              <hgroup class="page__hgroup">
                <?php the_title( '<h1 class="primary-title">', '</h1>' ); ?>
                <?php if( get_field('post_subtitle') ) : ?>
                  <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
                <?php endif; ?>
              </hgroup>
            </header><!-- .page__header -->

            <div class="page__intro formatted-content">
              <?php echo get_field('about_intro_txt'); ?>
            </div>

          </div>
        </div>

        <div class="full-width-bg">
          <div class="grid-container--small">
            <img src="<?php bloginfo('template_url'); ?>/img/about.gif" alt="GIF dei soci di RAD - Agenzia di Comunicazione">
          </div>
            <a class="full-width-bg__btn" href="<?php bloginfo('url'); ?>/i-founders-di-rad/" title="<?php _e('Scopri i founders di RAD', 'ae'); ?>"><?php _e('Scopri i founders di RAD', 'ae'); ?> <i class="iconae regular ae--arrow-right"></i></a>
        </div>

        <div class="grid-container--small">

          <div class="page__content formatted-content">
            <?php the_content(); ?>
          </div><!-- .page__content -->

        </div>

      </article><!-- #post-## -->

      <section class="clients">
        <div class="grid-container--large">
          <div class="row columns-middle">
            <?php
            $args = array( 'post_type' => 'partners', 'posts_per_page' => 99 );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="row__column desk-2 tab-4 mobile-6">
              <article class="client__single">
                <?php
                if ( checkCookiesSet() == 'negative' ) {
                  echo '<img src="' . get_field('negative_partners') . '" alt="Clienti Rad - '. get_the_title() .'" data-id="' . $post->ID . '" data-acf="negative_partners" class="p_n_dynamic">';
                } else {
                  echo get_the_post_thumbnail($post->ID, 'full', array( 'class' => 'p_n_dynamic', 'data-id' => $post->ID, 'data-acf' => 'negative_partners' ));
                }
                ?>
              </article>
            </div>
            <?php endwhile; ?>
          </div>
        </div>
      </section>

    <?php endwhile; ?>
</main><!-- #main -->
 <?php get_footer(); ?>
