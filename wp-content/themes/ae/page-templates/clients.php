<?php
/**
 * The template to display clients.
 *
 * Template Name: Clients
 *
 * This is the template that displays Clients.
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
  <article id="post-<?php the_ID(); ?>" <?php post_class('page'); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">

    <div class="overflow-hidden js--full-height">
      <div class="canvas-container">
        <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
      </div>

      <div class="grid-container--small vertical-align">

        <header class="page__header">
          <hgroup class="page__hgroup">
            <h1 class="primary-title">
              <div class="glitch-black" data-text="<?php the_title(); ?>">
                <?php the_title(); ?>
              </div>
            </h1>
            <?php if( get_field('post_subtitle') ) : ?>
              <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
            <?php endif; ?>
          </hgroup>
        </header><!-- .page__header -->

        <div class="page__intro formatted-content">
          <?php echo get_field('about_intro_txt'); ?>
          <a class="rad-btn" href="<?php echo get_permalink(40); ?>" title="<?php _e('I Founders di RAD', 'ae'); ?>">
            <?php _e('Leggi il manifesto', 'ae'); ?>
          </a>
        </div>

      </div>

      <div class="arrow-animotion grid-container--large js--arrow-animotion txt-center">
        <a class="arrow-animotion__link js--arrow-animotion-link" href="#start-clients">
          <div class="arrow-animotion__dynamic">
            <svg x="0px" y="0px" width="22px" height="29px">
              <path d="M21.7,17.3c-0.4-0.4-1-0.4-1.4,0L12,25.6V1c0-0.6-0.4-1-1-1s-1,0.4-1,1v24.6l-8.3-8.3c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4
              l10,10c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l10-10C22.1,18.3,22.1,17.7,21.7,17.3z"/>
            </svg>
          </div>
        </a>
      </div>

    </div>

  </article><!-- #post-## -->

  <section class="clients" id="start-clients">

    <div class="row--stuck columns-middle">
      <?php
      $args = array( 'post_type' => 'clients', 'posts_per_page' => 99 );
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <div class="row__column tab-4 mobile-6">
        <article class="client__single js--rad-waypoint fade--in-up" data-offset_delay="10">
          <?php echo '<img src="' . get_field('negative_clients') . '" data-id="' . $post->ID . '" alt="Clienti Rad - '. get_the_title() .'">'; ?>
        </article>
      </div>
      <?php endwhile; ?>
    </div>
  </section>

  <section class="works-count txt-center">
    <span class="works-count__txt">Stiamo lavorando a: </span> <span class="works-count__number">
      <i class="works-count__number-current wow tada" data-wow-iteration="infinite">5</i>/<i class="works-count__number-small">8</i></span> <span class="works-count__txt"> Progetti
      </span>
  </section>

    <?php
    while ( have_posts() ) : the_post();

      get_template_part( 'template-parts/content', 'call_to_action' );

      // If comments are open or we have at least one comment, load up the comment template.
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;

    endwhile; // End of the loop.
    ?>

</main><!-- #main -->

</div>
<?php get_footer(); ?>
