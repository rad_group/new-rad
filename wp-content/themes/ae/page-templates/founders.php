<?php
/**
 * The template to display Founders.
 *
 * Template Name: Founders
 *
 * This is the template that displays Founders.
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">
  <?php while ( have_posts() ) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class('page'); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">

      <div class="overflow-hidden">
        <div class="canvas-container">
          <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
        </div>

        <div class="grid-container--small">

          <header class="page__header">
            <hgroup class="page__hgroup">
              <?php the_title( '<h1 class="primary-title">', '</h1>' ); ?>
              <?php if( get_field('post_subtitle') ) : ?>
                <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
              <?php endif; ?>
            </hgroup>
          </header><!-- .page__header -->

          <div class="page__intro formatted-content">
            <?php echo get_field('about_intro_txt'); ?>
          </div>

        </div>
      </div>

      <div class="grid-container--small">

          <?php for ($i=1; $i <= 4; $i++) : ?>

            <div class="single-founder">
              <hgroup>
                <h2 class="prefix-title">
                  <?php echo get_field( 'founders_title_' . $i ); ?>
                </h2>
                <h3 class="single-founder__duty">
                  <?php echo get_field( 'founders_duty_' . $i ); ?>
                </h3>
              </hgroup>
              <div class="formatted-content">
                <p><?php echo get_field( 'founders_desc_' . $i . '', false, false  ); ?></p>
              </div>
            </div>

          <?php endfor; ?>

      </div>

    </article><!-- #post-## -->


  <?php endwhile; ?>
</main><!-- #main -->
<?php get_footer(); ?>
