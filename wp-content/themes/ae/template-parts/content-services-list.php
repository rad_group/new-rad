<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>
<?php
  $args = array(
    'post_type' => 'servicefolder',
    'posts_per_page' => -1
  );
  $loop = new WP_Query( $args );
?>
<section class="services-band">
  <ul class="services-band__list js--services-band">
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <li class="services-band__list-item"><?php the_title(); ?></li>
    <?php endwhile; ?>
  </ul>
</section>
