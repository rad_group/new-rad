<?php
/**
 * Template part for displaying posts on blog page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post-preview'); ?> itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">

  <a class="animsition-link" href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark">

    <div class="post-preview__container">

      <div class="post-preview__img">
        <img class="js--lazy" src="<?php bloginfo('template_url'); ?>/img/preload.gif" data-original="<?php echo get_the_post_thumbnail_url($post, 'full'); ?>" alt="<?php the_title(); ?> Thumbnail">
      </div>

      <div class="post-preview__txt">
        <div class="post-preview__txt-wrap vertical-align">
          <header class="post-preview__header">
            <h2 class="post-preview__title">
              <?php the_title(); ?>
            </h2> <!-- .post-preview__title -->
          </header><!-- .post-preview__header -->

          <?php the_excerpt(); ?>
        </div>

      </div>

    </div>

  </a>

</article><!-- #post-## -->
