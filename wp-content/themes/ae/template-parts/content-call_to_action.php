<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<section class="call-to-action">
  <div class="canvas-container">
    <canvas class="js--particles particles" data-color="<?php if( checkCookiesSet() == 'negative' ) { echo 'white'; } else { echo 'black'; } ?>" height="1280"></canvas>
  </div>
    
</section>

<div class="js--popup popup--small contacts-popup" id="contact-form">

  <div class="popup__wrap vertical-align">
    <?php  echo do_shortcode('[contact-form-7 id="538" title="Contact Form"]'); ?>
    <button class="js--close-popup popup__button">×</button>
  </div>

</div>
