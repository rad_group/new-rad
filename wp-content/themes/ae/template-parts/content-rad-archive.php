<section class="rad-archive bg-screen color-switcher__black">
  <div class="rad-archive__background"></div>
  <div class="rad-archive__content">
    <div class="grid-container--large">
      <h2 class="rad-archive__title"><?php _e('Rad archive', 'ae'); ?></h2>
      <h3 class="rad-archive__cta"><?php _e('Get access to our projects archive', 'ae'); ?></h3>
      <span class="rad-archive__link js--popup-action" data-popup="rad-archive-popup">
        <?php _e('Discover', 'ae'); ?>
      </span>
    </div>
  </div>
</section>

<div class="js--popup popup archive-poup bg-screen color-switcher__white" id="rad-archive-popup">
  <div class="grid-container--small">
    <div class="archive-poup__wrap">
      <div class="archive-poup__header">
        <h2 class="archive-poup__header-title">L'archivio completo dei nostri lavori</h2>
        <p>Inviaci la richiesta per poter visionare l'archivio completo di tutti i nostri lavori, compilando il form sottostante.</p>
      </div>
      <div class="archive-poup__container">
        <?php echo do_shortcode('[contact-form-7 id="6760" title="Archive Form"]'); ?>
      </div>
      <button class="js--close-popup popup__button">CLOSE</button>
    </div>
  </div>
</div>
