<?php if( have_rows('work_flex') ) : ?>

  <?php while ( have_rows('work_flex') ) : the_row(); ?>

    <?php if( get_row_layout() == 'primary_content' ) : ?>

      <div class="composer__block <?php echo get_margin() . ' ' . change_logo(); ?> js--rad-waypoint fade--in-up on-screen">

      <?php $selected_columns = composer_check();

      switch ( $selected_columns ) :

        // Only text selected
        case 0: ?>

          <div class="grid-container">
            <?php get_composer_txt(); ?>
          </div>

        <?php break;

        // Only image selected
        case 1: ?>

          <?php
          $position = get_sub_field_object('flex_primary_content_only_image_layout');
          $value = $position['value'];

          switch ($value) :
            case 'content_only_image_layout_left': // ONLY IMG LEFT ?>
              <div class="row--stuck">
                <div class="row__column tab-6">
                  <?php echo get_composer_img('first'); ?>
                </div>
              </div>
            <?php break;

            case 'content_only_image_layout_right': // ONLY IMG RIGHT ?>
              <div class="row--stuck">
                <div class="row__column tab-push-6 tab-6">
                  <?php get_composer_img('first'); ?>
                </div>
              </div>
            <?php break;

            case 'content_only_image_layout_middle': // ONLY IMG MIDDLE ?>
              <div class="grid-container--large">
                <?php get_composer_img('first'); ?>
              </div>
            <?php break;

            case 'content_only_image_layout_full': // ONLY IMG FULL ?>
              <?php get_composer_img('first'); ?>
            <?php break;

            case 'content_only_image_layout_double': // DOUBLE ?>
              <div class="row--stuck">
                <div class="row__column tab-6">
                  <?php get_composer_img('first'); ?>
                </div>
                <div class="row__column tab-6">
                  <?php get_composer_img('second'); ?>
                </div>
              </div>
            <?php break; ?>
          <?php endswitch; ?>

        <?php break;

        // Text and image selected
        case 2:

          $position = get_sub_field_object('flex_primary_content_image_txt_layout');
          $value = $position['value'];

          switch ($value) {
            case 'content_image_txt_layout_left': // IMG+TXT LEFT ?>
              <div class="row--stuck columns-middle">
                <div class="row__column tab-6 color-switcher__black">
                  <?php get_composer_img('first'); ?>
                </div>
                <div class="row__column tab-6">
                  <div class="composer__txt-only-wrap">
                    <?php get_composer_txt(); ?>
                  </div>
                </div>
              </div>
            <?php break;

            case 'content_image_txt_layout_right': // IMG+TXT RIGHT ?>
              <div class="row--stuck columns-middle">
                <div class="row__column tab-6">
                  <div class="composer__txt-only-wrap">
                    <?php get_composer_txt(); ?>
                  </div>
                </div>
                <div class="row__column tab-6">
                  <?php get_composer_img('first'); ?>
                </div>
              </div>
            <?php break;
          } ?>

        <?php break;
      endswitch; ?>

    </div>
    <?php endif; ?>
    <?php if( get_row_layout() == 'flex_carousel' ) : ?>
      <div class="composer__block <?php echo get_margin(); ?>">
        <?php
        $images = get_sub_field('flex_carousel_gallery');
        $gyroscope = 'vertical';
        if( get_sub_field('flex_carousel_gallery_check') == true ) {
          $gyroscope = 'horizontal';
        };
        ?>

        <?php if ( $images ) : ?>

          <div class="composer__carousel js--composer-carousel--<?php echo $gyroscope; ?> owl-carousel">
            <?php
            $position = 1;
            foreach( $images as $image ):

              if ( $image['alt'] ) {
                $image_alt = $image['alt'];
              } else {
                $image_alt = 'We RAD - Agenzia di Comunicazipne - Immagine del lavoro';
              } ?>
              <figure class="composer__carousel-figure">
                <img class="composer__carousel-single-img lazy <?php echo $gyroscope; ?>" data-src="<?php echo $image['url']; ?>" src="<?php echo $image['url']; ?>" alt="<?php echo $image_alt; ?>" >
              </figure>

              <?php $position++; ?>
            <?php endforeach; ?>
          </div>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php if( get_row_layout() == 'flex_video' ) : ?>
      <div class="composer__block <?php echo get_margin() . ' ' . change_logo(); ?>">
        <?php $overlay_txt = get_sub_field('flex_video_overlay_txt'); ?>
        <?php if( have_rows('flex_video_group') ): ?>
          <?php while( have_rows('flex_video_group') ): the_row(); ?>

            <?php if ( get_sub_field('flex_video_layout') == 'middle' ) {
              $video_grid = 'grid-container--large';
            } else {
              $video_grid = null;
            }?>

            <div class="composer__video <?php echo $video_grid; ?>">
              <?php
              $video_autoplay = get_sub_field('flex_video_autoplay');
              $video_mute = get_sub_field('flex_video_mute');
              $video_poster = get_sub_field('flex_video_poster');
              $video_mp4 = get_sub_field('flex_video_mp4');
              $video_webm = get_sub_field('flex_video_webm');

              if ( $video_autoplay ) {
                $flex_autoplay = 'autoplay loop playsinline';
              } else {
                $flex_autoplay = null;
              }

              if ( $video_mute ) {
                $flex_mute = 'muted';
              } else {
                $flex_mute = null;
              }
              ?>

              <section class="home-video">
                <div class="video-ui js--video-ui">
                  <?php if ( $overlay_txt ) : ?>
                    <div class="video__overlay">
                      <h2 class="composer__txt-title--white vertical-align grid-container txt-center">
                        <?php echo $overlay_txt; ?>
                      </h2>
                    </div>
                  <?php endif; ?>
                  <video class="video-ui__video" preload="metadata" poster="<?php echo $video_poster; ?>" <?php echo $flex_autoplay; ?> <?php echo $flex_mute; ?>>
                    <source src="<?php echo $video_mp4; ?>" type="video/mp4">
                    <source src="<?php echo $video_webm; ?>" type="video/webm">
                  </video>
                  <?php if ( $video_autoplay != true) : ?>
                  <div class="video-ui__playpause">
                    <img src="<?php bloginfo('template_url'); ?>/img/rad-play.svg" alt="ui-video-play">
                  </div>
                <?php endif; ?>
                </div>
              </section>

            </div>

          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php if( get_row_layout() == 'flex_devices' ) : ?>
      <div class="composer__block <?php echo get_margin() . ' ' . change_logo(); ?>">
        <?php
        $c = 1;
        $col = 12;
        $device = 'single';
        if(get_sub_field('flex_devices_columns') == true ) :
          $c = 2;
          $col = 6;
          $device = 'double';
        endif;
        ?>
        <?php if( have_rows('flex_devices_group') ): ?>
          <div class="row--stuck columns-middle">
            <div class="row__column tab-<?php echo $col; ?>">

              <?php while( have_rows('flex_devices_group') ): the_row();
                $device_model = get_sub_field('flex_devices_model');

                switch ($device_model) {
                  case 'desktop': ?>
                    <div class="composer__container" style="background-color: <?php echo get_sub_field('flex_devices_background_first'); ?>">
                      <div class="grid-container--large desk-left <?php echo $device; ?>">
                        <div class="composer__devices-wrap--desk js--rad-waypoint fade--in-up on-screen">
                          <img class="lazy" data-src="<?php bloginfo('template_url'); ?>/img/device_template/rad_thunderbolt_display.png" src="<?php bloginfo('template_url'); ?>/img/device_template/rad_thunderbolt_display.png" alt="Siti Web Rad - Anteprima Desktop">
                          <div class="composer__devices-screen--desk">
                            <?php get_devices_media_type(); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php break;

                  case 'tablet': ?>
                    <div class="composer__container" style="background-color: <?php echo get_sub_field('flex_devices_background_first'); ?>">
                      <div class="grid-container tab-left <?php echo $device; ?>">
                        <div class="composer__devices-wrap--tab js--rad-waypoint fade--in-up on-screen">
                          <img class="lazy device-tab" data-src="<?php bloginfo('template_url'); ?>/img/device_template/rad_ipad_pro.png" src="<?php bloginfo('template_url'); ?>/img/device_template/rad_ipad_pro.png" alt="Siti Web Rad - Anteprima Tablet">
                          <div class="composer__devices-screen--tab">
                            <?php get_devices_media_type(); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php break;

                  case 'mobile': ?>
                    <div class="composer__container"style="background-color: <?php echo get_sub_field('flex_devices_background_first'); ?>">
                      <div class="grid-container--small mobile-left <?php echo $device; ?>">
                        <div class="composer__devices-wrap--mobile js--rad-waypoint fade--in-up on-screen">
                          <img class="lazy device-mobile" data-src="<?php bloginfo('template_url'); ?>/img/device_template/rad_iphone8.png" src="<?php bloginfo('template_url'); ?>/img/device_template/rad_iphone8.png" alt="Siti Web Rad - Anteprima Mobile">
                          <div class="composer__devices-screen--mobile">
                            <?php get_devices_media_type(); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php break;
                } ?>
              <?php endwhile; ?>
            </div>

            <?php if(get_sub_field('flex_devices_columns') == true): ?>
              <div class="row__column tab-6">
                <?php while( have_rows('flex_devices_group_duplicate') ): the_row();
                  $device_model = get_sub_field('flex_devices_model');
                  switch ($device_model) {
                    case 'desktop': ?>
                      <div class="composer__container" style="background-color: <?php echo get_sub_field('flex_devices_background_second'); ?>">
                        <div class="grid-container desk-right <?php echo $device; ?>">
                          <div class="composer__devices-wrap--desk js--rad-waypoint fade--in-up on-screen">
                            <img class="lazy" data-src="<?php bloginfo('template_url'); ?>/img/device_template/rad_thunderbolt_display.png" src="<?php bloginfo('template_url'); ?>/img/device_template/rad_thunderbolt_display.png" alt="Siti Web Rad - Anteprima Desktop">
                            <div class="composer__devices-screen--desk">
                              <?php get_devices_media_type(); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php break;

                    case 'tablet': ?>
                      <div class="composer__container" style="background-color: <?php echo get_sub_field('flex_devices_background_second'); ?>">
                        <div class="grid-container tab-right <?php echo $device; ?>">
                          <div class="composer__devices-wrap--tab js--rad-waypoint fade--in-up on-screen">
                            <img class="lazy device-tab" data-src="<?php bloginfo('template_url'); ?>/img/device_template/rad_ipad_pro.png" src="<?php bloginfo('template_url'); ?>/img/device_template/rad_ipad_pro.png" alt="Siti Web Rad - Anteprima Tablet">
                            <div class="composer__devices-screen--tab">
                              <?php get_devices_media_type(); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php break;

                    case 'mobile': ?>
                      <div class="composer__container" style="background-color: <?php echo get_sub_field('flex_devices_background_second'); ?>">
                        <div class="grid-container--small mobile-right <?php echo $device; ?>">
                          <div class="composer__devices-wrap--mobile js--rad-waypoint fade--in-up on-screen">
                            <img class="lazy device-mobile" data-src="<?php bloginfo('template_url'); ?>/img/device_template/rad_iphone8.png" src="<?php bloginfo('template_url'); ?>/img/device_template/rad_iphone8.png" alt="Siti Web Rad - Anteprima Mobile">
                            <div class="composer__devices-screen--mobile">
                              <?php get_devices_media_type(); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php break;
                  } ?>
                <?php endwhile; ?>
              </div>
            <?php endif; ?>

          </div>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php if( get_row_layout() == 'flex_awards' ) : ?>
      <div class="composer__block <?php echo get_margin(); ?> color-switcher__white">
        <div class="awards">
          <div class="grid-container--large">
            <div class="awards__container">
              <div class="awards__box">
                <div class="awards__text">
                  <h3 class="awards__title js--rad-waypoint fade--in-up on-screen"><?php echo get_sub_field('awards_title'); ?></h3>
                  <p class="awards__description js--rad-waypoint fade--in-up on-screen"><?php echo get_sub_field('awards_description'); ?></p>
                </div>
              </div>
              <?php if( have_rows('awards_repeater') ): ?>
                <?php while( have_rows('awards_repeater') ): the_row(); ?>
                  <div class="awards__box">
                    <img class="awards__rewarder lazy js--rad-waypoint fade--in-up on-screen" data-src="<?php echo get_sub_field('awards_site'); ?>" src="<?php echo get_sub_field('awards_site'); ?>" alt="Award logo">
                    <?php while( have_rows('awards_prize_list')): the_row(); ?>
                      <span class="awards__prize-cat js--rad-waypoint fade--in-up on-screen"><?php echo get_sub_field('awards_prize'); ?></span>
                    <?php endwhile; ?>
                  </div>
                <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if( get_row_layout() == 'flex_font' ) : ?>
      <div class="composer__block <?php echo get_margin(); ?> color-switcher__black">
        <div class="font">
          <div class="grid-container--large">
            <div class="font__container">
              <?php
              $font_background = get_sub_field('font_background');
              $font_negative = get_sub_field('font_color_negative');
              $font_class = get_sub_field('font_class');
              if( have_rows('font_repeater') ): ?>
                <hgroup>
                  <h3 class="composer__txt-prefix js--rad-waypoint fade--in-up on-screen">FONT</h3>
                  <h2 class="composer__txt-title js--rad-waypoint fade--in-up on-screen"><?php _e('The typographic rules of', 'ae'); ?> <?php echo get_the_title(); ?></h2>
                </hgroup>
                <div class="row--stuck columns-middle">
                  <?php while(have_rows('font_repeater') ): the_row(); ?>
                    <div class="row__wrapper">
                      <div class="row__column tab-3 desk-4">
                        <hgroup class="font__chosen">
                          <h3 class="js--rad-waypoint fade--in-up on-screen"><?php echo get_sub_field('font_chosen'); ?></h3>
                          <h2 class="js--rad-waypoint fade--in-up on-screen"><?php echo get_sub_field('font_type'); ?></h2>
                        </hgroup>
                      </div>
                      <div class="row__column tab-9 desk-8">
                        <div class="font__sample js--rad-waypoint fade--in-up on-screen">
                          <img src="<?php echo get_sub_field('font_sample'); ?>" alt="<?php echo get_sub_field('font_type');  ?>">
                        </div>
                      </div>
                    </div>
                  <?php endwhile; ?>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>


    <?php if( get_row_layout() == 'flex_palette' ) : ?>
      <div class="composer__block <?php echo get_margin(); ?> js--color-palette js--rad-waypoint fade--in-up on-screen">
        <div class="color-palette color-switcher__black">
          <div class="grid-container">
            <hgroup class="color-palette__intro">
              <h3 class="color-palette__pre-title js--rad-waypoint fade--in-up on-screen"><?php echo get_sub_field('flex_palette_pretitle'); ?></h3>
              <h2 class="color-palette__title js--rad-waypoint fade--in-up on-screen"><?php echo get_sub_field('flex_palette_title'); ?></h2>
              <p class="color-palette__description js--rad-waypoint fade--in-up on-screen"><?php echo get_sub_field('flex_palette_description'); ?></p>
            </hgroup>
            <div class="color-palette__choices">
              <?php if( have_rows('flex_palette_repeater')): ?>
                <div class="row--half">
                  <?php while(have_rows('flex_palette_repeater')): the_row(); ?>
                    <?php
                    $contrast = '';
                    if( get_sub_field('flex_palette_color_switch') == true ){
                      $contrast = 'contrast';
                    }
                    ?>
                    <div class="row__column mobile-6 tab-4 desk-3">
                      <div class="color-palette__box  <?php echo $contrast; ?>">
                        <div class="color-palette__color" style="background-color:<?php echo get_sub_field('flex_palette_color'); ?>" data-color="<?php echo get_sub_field('flex_palette_color'); ?>">
                        </div>
                        <h4 class="color-palette__color-name"><?php echo get_sub_field('flex_palette_color_name'); ?></h4>
                      </div>
                    </div>
                  <?php endwhile; ?>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

  <?php endwhile; ?>

<?php endif; ?>

<?php
/*
 * Check if selected columns (txt, img or both)
 * $selected == 0 --> Only Text
 * $selected == 1 --> Only Image
 * $selected == 2 --> Image and Text
 */
function composer_check() {
  $check_columns = get_sub_field_object('primary_content_columns');
  $value = $check_columns['value'];

  if ( count($value) > 1 ) {
    $selected = 2;
  } else {
    for ($c = 0; $c <= count($value); $c++) {
      if ( isset($value[$c]) && $value[$c] == 'single_txt' ) {
        $selected = 0;
      } else if ( isset($value[$c]) && $value[$c] == 'single_img' ) {
        $selected = 1;
      }
    }
  }

  return $selected;
}

function get_composer_txt() {

  if( have_rows('flex_txt_group') ):
    while( have_rows('flex_txt_group') ): the_row();

      $prefix_title = get_sub_field('flex_primary_content_prefix');
      $primary_title = get_sub_field('flex_primary_content_title');
      $underscore = get_sub_field('flex_primary_content_title_underscore');
      $standard_txt = get_sub_field('flex_primary_content_txt');
      ?>

      <div class="composer__txt-only js--rad-waypoint fade--in-up on-screen">
        <hgroup>
          <?php if ( $prefix_title ) : ?>
            <h3 class="composer__txt-prefix js--rad-waypoint fade--in-up on-screen"><?php echo $prefix_title; ?></h3>
          <?php endif; ?>

          <?php if ( $primary_title ) : ?>
            <h2 class="composer__txt-title js--rad-waypoint fade--in-up on-screen"><?php echo $primary_title; ?></h2>
          <?php endif; ?>
        </hgroup>
        <?php if ( $underscore ) : ?>
          <div class="composer__txt-underscore"></div>
        <?php endif; ?>

        <?php if ( $standard_txt ) : ?>
          <div class="composer__txt-only--content formatted-content js--rad-waypoint fade--in-up on-screen">
            <?php echo $standard_txt; ?>
          </div>
        <?php endif; ?>
      </div>

    <?php
    endwhile;
  endif;

}

function get_composer_img($im) {

  if( have_rows('flex_img_group') ):
    while( have_rows('flex_img_group') ): the_row();

      $first_img = get_sub_field('flex_primary_content_img');
      $second_img = get_sub_field('flex_primary_content_img_second');
      ?>

      <div class="composer__img-wrap js--rad-waypoint fade--in-up on-screen">
        <?php
        if ( $im == 'first' ) {
          if ( $first_img ) {
            echo '<img class="composer__img lazy js--rad-waypoint fade--in-up on-screen" src="' . $first_img . '" data-src="'. $first_img .'">';
          }
        } elseif ( $im == 'second' ) {
          if ( $second_img ) {
            echo '<img class="composer__img lazy js--rad-waypoint fade--in-up on-screen" src="' . $second_img . '" data-src="'. $second_img .'">';
          }
        }
        ?>
      </div>

    <?php
    endwhile;
  endif;

}

function get_devices_media_type() {

  //General
  $media_type = get_sub_field('flex_devices_media_type');
  $static_img = get_sub_field('flex_devices_img_static');
  $video_mp4 = get_sub_field('flex_devices_video_mp4');
  $video_webm = get_sub_field('flex_devices_video_webm');
  $video_poster = get_sub_field('flex_devices_video_poster');

  if ( $media_type == 'img' ) {
    if ( $static_img['alt'] ) {
      $static_alt = $static_img['alt'];
    } else {
      $static_alt = 'We RAD - Agenzia di Comunicazione - Immagine del lavoro';
    } ?>
    <img class="lazy" src="<?php echo $static_img['url']; ?>" data-src="<?php echo $static_img['url']; ?>" alt="<?php echo $static_alt; ?>">
  <?php
  } else { ?>
    <video class="composer__devices-video lazy" preload="metadata" poster="<?php echo $video_poster; ?>" autoplay loop muted playsinline>
      <source data-src="<?php echo $video_mp4; ?>" type="video/mp4">
      <source data-src="<?php echo $video_webm; ?>" type="video/webm">
    </video>
  <?php
  }
}

function get_margin() {

  $margin_top = get_sub_field('flex_margin_top');
  $margin_bottom = get_sub_field('flex_margin_bottom');

  if ( $margin_top ) {
    $margin_top_class = 'composer__margin-top--' . $margin_top;
  } else {
    $margin_top_class = null;
  }

  if ( $margin_bottom ) {
    $margin_bottom_class = 'composer__margin-bottom--' . $margin_bottom;
  } else {
    $margin_bottom_class = null;
  }

  return $margin_top_class . ' ' . $margin_bottom_class;

}

function change_logo(){

  $logo_check = get_sub_field('logo_color_change');
  $switch_logo = 'color-switcher__black';

  if($logo_check == true){
    $switch_logo = 'color-switcher__white';
  }

  return $switch_logo;

}
