<?php
/**
 * Template part for displaying single posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

  <section id="long" class="dna js--dna">
    <video id="video" autoplay loop muted playsinline src="<?php bloginfo('template_directory'); ?>/video/testv11.mp4">
    </video>
  </section>
