<?php
/**
 * Template part for displaying single posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?> itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">

  <div class="overflow-hidden js--full-height color-switcher__black">
    <div class="canvas-container">
      <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
    </div>

    <div class="grid-container--large">

      <header class="page__header">

        <div class="single-post__thumb">
          <?php echo get_the_post_thumbnail($post, 'full'); ?>
        </div>

        <?php if ( 'post' === get_post_type() ) : ?>
          <div class="single-post__meta txt-center">
            <?php ae_posted_on(); ?>
          </div><!-- .single-post__meta -->
        <?php endif; ?>

        <hgroup class="page__hgroup txt-center">
          <?php the_title( '<h1 class="primary-title">', '</h1>' ); ?>
          <?php if( get_field('post_subtitle') ) : ?>
            <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
          <?php endif; ?>
        </hgroup>
      </header><!-- .page__header -->

      <div class="page__intro formatted-content">
        <?php echo get_field('about_intro_txt'); ?>
      </div>

    </div>

    <div class="arrow-animotion grid-container--large js--arrow-animotion txt-center">
      <a class="arrow-animotion__link js--arrow-animotion-link" href="#start-post">
        <div class="arrow-animotion__dynamic">
          <svg x="0px" y="0px" width="22px" height="29px">
            <path d="M21.7,17.3c-0.4-0.4-1-0.4-1.4,0L12,25.6V1c0-0.6-0.4-1-1-1s-1,0.4-1,1v24.6l-8.3-8.3c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4
            l10,10c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l10-10C22.1,18.3,22.1,17.7,21.7,17.3z"/>
          </svg>
        </div>
      </a>
    </div>

  </div>
  <div class="single-post__container color-switcher__black bg-screen">
    <div class="grid-container--small">
      <div class="formatted-content">
        <div class="single-post__content" id="start-post">
          <?php
          the_content( sprintf(
            /* translators: %s: Name of current post. */
            wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'ae' ), array( 'span' => array( 'class' => array() ) ) ),
            the_title( '<span class="screen-reader-text">"', '"</span>', false )
          ) );

          wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ae' ),
            'after'  => '</div>',
          ) );
          ?>
        </div><!-- .single-post__content -->
      </div>
    </div>
  </div>

</article><!-- #post-## -->
