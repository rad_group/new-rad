<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>
  <header class="single-post__header">
    <h2 class="single-post__title">
      <?php the_title( sprintf( '<a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' ); ?>
    </h2><!-- .single-post__title -->

    <?php if ( 'post' === get_post_type() ) : ?>
      <div class="single-post__meta">
        <?php ae_posted_on(); ?>
      </div><!-- .single-post__meta -->
    <?php endif; ?>
  </header><!-- .single-post__header -->

  <div class="single-post__summary">
    <?php the_excerpt(); ?>
  </div><!-- .single-post__summary -->

  <footer class="single-post__footer">
    <?php ae_entry_footer(); ?>
  </footer><!-- .single-post__footer -->

</article><!-- #post-## -->
