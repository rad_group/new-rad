<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('page'); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">

  <div class="overflow-hidden color-switcher__black">
    <div class="canvas-container">
      <canvas class="js--particles particles" data-color="white" height="1280" width="1680"></canvas>
    </div>

    <div class="grid-container--small">

      <header class="page__header">
        <hgroup class="page__hgroup">
          <h1 class="primary-title">
            <div class="glitch-black" data-text="<?php the_title(); ?>">
              <?php the_title(); ?>
            </div>
          </h1>
          <?php if( get_field('post_subtitle') ) : ?>
            <h2 class="secondary-title"><?php echo get_field('post_subtitle'); ?></h2>
          <?php endif; ?>
        </hgroup>
      </header><!-- .page__header -->

      <div class="page__intro formatted-content">
        <?php echo get_field('about_intro_txt'); ?>
      </div>

    </div>
  </div>

  <div class="grid-container--small">
    <div class="formatted-content">

      <div class="indent-2 page__content">
        <?php the_content(); ?>
      </div><!-- .page__content -->

    </div>
  </div>

</article><!-- #post-## -->
