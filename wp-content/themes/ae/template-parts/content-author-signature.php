<?php
$user_id = get_the_author_meta('ID');
$user_nice = get_the_author_meta('nickname');
?>
<section class="blog-author">
  <div class="overflow-hidden">
    <div class="canvas-container">
      <canvas class="js--particles particles" data-color="<?php if( checkCookiesSet() == 'negative' ) { echo 'black'; } else { echo 'white'; } ?>" height="1280"></canvas>
    </div>

    <div class="blog-author__wrap">
      <div class="grid-container">
        <div class="row">

          <div class="row__column tab-4 desk-3 txt-center">
            <div class="blog-author__avatar">
              <?php if ( get_field('author_avatar', 'user_'.$user_id) ) : ?>
                <img class="blog-author__avatar-img" src="<?php echo get_field('author_avatar', 'user_'.$user_id); ?>" alt="Immagine profilo di <?php echo get_the_author_meta('nickname'); ?>">
              <?php else : ?>
                <img class="blog-author__avatar-img" src="<?php bloginfo('template_directory'); ?>/img/RAD-wp-avatar.png" alt="Immagine profilo di RAD">
              <?php endif; ?>
            </div>
          </div>

          <div class="row__column tab-8 desk-9">
            <h3 class="blog-author__nickname">
              <a href="<?php echo get_author_posts_url( $user_id ); ?>" rel="author" title="Autore RAD - <?php echo get_the_author_meta('nickname'); ?>">
                <?php echo get_the_author_meta('nickname'); ?>
              </a>
            </h3>
            <?php if ( get_field('author_job', 'user_'.$user_id) ) : ?>
              <h4 class="blog-author__job">
                <?php echo get_field('author_job', 'user_'.$user_id); ?>
              </h4>
            <?php endif; ?>

            <nav class="blog-author__social">
              <?php if ( get_the_author_meta( 'user_url', $post->post_author ) ) : ?>
                <a href="<?php echo get_the_author_meta( 'user_url', $post->post_author ); ?>" title="Sito personale di <?php echo get_the_author_meta('nickname'); ?>" target="_blank">
                  <i class="iconae light ae--globe"></i>
                </a>
              <?php endif; ?>

              <?php if ( get_field('social_author_linkedin', 'user_'.$user_id) ) : ?>
                <a href="<?php echo get_field('social_author_linkedin', 'user_'.$user_id); ?>" title="Profilo Linkedin personale di <?php echo get_the_author_meta('nickname'); ?>" target="_blank">
                  <i class="iconae light ae--linkedin-square"></i>
                </a>
              <?php endif; ?>

              <?php if ( get_field('social_author_instagram', 'user_'.$user_id) ) : ?>
                <a href="<?php echo get_field('social_author_instagram', 'user_'.$user_id); ?>" title="Profilo Instagram personale di <?php echo get_the_author_meta('nickname'); ?>" target="_blank">
                  <i class="iconae light ae--instagram"></i>
                </a>
              <?php endif; ?>

              <?php if ( get_the_author_meta( 'googleplus',$post->post_author) ) : ?>
                <a href="<?php echo get_the_author_meta( 'googleplus',$post->post_author);?>" title="Pagina Google Plus di <?php echo get_the_author_meta('nickname'); ?>" target="_blank" rel="me">
                  <i class="iconae light ae--google-plus-square"></i>
                </a>
              <?php endif; ?>

            </nav>

            <div class="blog-author__desc formatted-content">
              <p>
                <?php echo get_the_author_meta("description"); ?>
              </p>
            </div>

          </div>
        </div>

      </div>
    </div>

  </div>
</section>
