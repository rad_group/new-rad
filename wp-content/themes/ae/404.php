<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package framaework
 */

 get_header(); ?>

 <div class="canvas-container">
   <canvas class="js--particles particles" data-color="<?php if( checkCookiesSet() == 'negative' ) { echo 'black'; } else { echo 'white'; } ?>" height="1280"></canvas>
 </div>

 <div class="grid-container">
   <main id="main" class="site-main js--full-height-content p-relative" role="main" role="main" itemprop="mainContentOfPage">

     <article itemscope="itemscope" itemtype="http://schema.org/CreativeWork">

      <div class="not-found-page vertical-align">
        404
      </div>

     </article><!-- #post-## -->

   </main><!-- #main -->

   </div> <!-- .row -->
 </div>
 <?php get_footer(); ?>
